
#pragma once

#include <types.hxx>
#include "action_schema.hxx"

namespace fs0 {

class ApplicableActionSet;
class BaseActionManager;

class GroundAction {
protected:
	unsigned _id;
	
	//! The values of the action parameters, if any.
	const std::vector<float> _binding;
	
	ActionSchema::cptr _schema;

public:
	typedef const GroundAction* cptr;
	
	//! Trait required by aptk::DetStateModel
	typedef ActionIdx IdType;
	typedef std::vector<GroundAction::IdType> ApplicableSet;

	static const ActionIdx invalid_action_id;
	
	GroundAction(unsigned id, const std::vector<float>& binding, ActionSchema::cptr schema)
		: _id(id), _binding(binding), _schema(schema) {}
		
	GroundAction(unsigned id, const std::vector<float>& binding)
		: _id(id), _binding(binding) {}
	
	~GroundAction();
	
	//! Returns the name of the action, e.g. 'move'
	const std::string& getName() const { return _schema->getName(); }
	
	//! Returns the full, grounded name of the action, e.g. 'move(b1, c2)'
	std::string getFullName() const;
	
	//! Returns the signature of the action
	const Signature& getSignature() const { return _schema->getSignature(); }
	
	//! Returns the concrete binding that created this action from its action schema
	const std::vector<float>& getBinding() const { return _binding; }
	
	//! Prints a representation of the object to the given stream.
	friend std::ostream& operator<<(std::ostream &os, const GroundAction&  entity) { return entity.print(os); }
	std::ostream& print(std::ostream& os) const;
};


} // namespaces
