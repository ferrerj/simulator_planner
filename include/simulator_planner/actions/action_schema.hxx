
#pragma once

#include <types.hxx>

namespace fs0 {

class ActionSchema {
protected:
	//! The name of the action
	const std::string _name;
	
	//! The signature of the action
	const Signature _signature;
	
	//! The name of the parameters
	const std::vector<std::string> _parameters;

public:
	typedef const ActionSchema* cptr;
	
	ActionSchema(const std::string& name, const Signature& signature, const std::vector<std::string>& parameters);
	~ActionSchema();
	
	inline const std::string& getName() const { return _name; }
	
	inline const Signature& getSignature() const { return _signature; }
	
	inline const std::vector<std::string>& getParameters() const { return _parameters; }

	//! Prints a representation of the object to the given stream.
	friend std::ostream& operator<<(std::ostream &os, const ActionSchema& o) { return o.print(os); }
	std::ostream& print(std::ostream& os) const;
};


} // namespaces
