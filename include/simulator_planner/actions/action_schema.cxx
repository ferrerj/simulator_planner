
#include <actions/action_schema.hxx>
#include <problem.hxx>
#include <actions/ground_action.hxx>
#include <utils/printers/binding.hxx>

namespace fs0 {

ActionSchema::ActionSchema(const std::string& name, const Signature& signature, const std::vector<std::string>& parameters)
	: _name(name), _signature(signature), _parameters(parameters)
{
	assert(parameters.size() == signature.size());
}

ActionSchema::~ActionSchema() {
}

std::ostream& ActionSchema::print(std::ostream& os) const { 
	os <<  _name << "(" << print::signature(_parameters, getSignature()) << ")" << std::endl;
	return os;
}

} // namespaces
