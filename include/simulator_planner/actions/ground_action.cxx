
#include <actions/ground_action.hxx>
#include <problem.hxx>
#include <limits>
#include <utils/printers/binding.hxx>
#include <utils/printers/actions.hxx>


namespace fs0 {

const ActionIdx GroundAction::invalid_action_id = std::numeric_limits<unsigned int>::max();

GroundAction::~GroundAction() {}

std::string GroundAction::getFullName() const {
     std::ostringstream stream;
     stream << getName() << "(" << print::binding(getBinding(), getSignature()) << ")";
     return stream.str();
}

std::ostream& GroundAction::print(std::ostream& os) const {
        os << "Action #" << _id;
	//os << print::action_name(*this) << std::endl;	
	return os;
}


} // namespaces
