
#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <functional>

#include <types.hxx>
#include <state.hxx>
#include <problem.hxx>

#include <lib/rapidjson/document.h>
#include <actions/action_schema.hxx>

namespace fs0 {

class Loader {
public:
	
	//!
	static std::unique_ptr<Problem> loadProblem(const rapidjson::Document& data, State&& init);
	
	static rapidjson::Document loadJSONObject(const std::string& filename);
	
protected:
	static std::vector<ActionSchema::cptr> loadActionSchemata(const rapidjson::Value& data);
	
	static std::vector<GroundAction::cptr> loadGroundedActions(const rapidjson::Value& data);
	
	static ActionSchema::cptr loadActionSchema(const rapidjson::Value& data);
	
	static std::vector<AtomRange> loadGoalConditions(const rapidjson::Value& data);
	//static Goals loadGoalConditions(const rapidjson::Value& data);

	
	// Conversion to a C++ vector of values.
	template<typename T>
	static std::vector<T> parseNumberList(const rapidjson::Value& data);
	static std::vector<std::string> parseStringList(const rapidjson::Value& data);
	
	static std::vector<float> parseDoubleParameters(const rapidjson::Value& data);
	
	template<typename T>
	static std::vector<std::vector<T>> parseDoubleNumberList(const rapidjson::Value& data);
};

} // namespaces

