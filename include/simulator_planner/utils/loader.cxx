
#include <memory>
#include <boost/lexical_cast.hpp>

#include <problem.hxx>
#include <utils/loader.hxx>
#include <actions/ground_action.hxx>
//#include <utils/logging.hxx>


namespace fs0 {

std::unique_ptr<Problem> Loader::loadProblem(const rapidjson::Document& data, State&& init) {
	
	/* Define the actions */
	auto schemata = loadActionSchemata(data["action_schemata"]);
	auto grounded = loadGroundedActions(data["grounded_actions"]);
	auto goal_conditions = loadGoalConditions(data["goal"]);

	auto problem = new Problem(data["domain"].GetString(), data["instance"].GetString(), schemata, grounded, init, goal_conditions);
	return std::unique_ptr<Problem>(problem);
}

std::vector<ActionSchema::cptr> Loader::loadActionSchemata(const rapidjson::Value& data) {
	std::vector<ActionSchema::cptr> schemata;
	for (unsigned i = 0; i < data.Size(); ++i) {
		schemata.push_back(loadActionSchema(data[i]));
	}
	return schemata;
}

std::vector<GroundAction::cptr> Loader::loadGroundedActions(const rapidjson::Value& data) {
	std::vector<GroundAction::cptr> actions;
	for(unsigned i = 0; i < data.Size(); ++i) 
	    actions.push_back(new GroundAction(data["id"].GetInt(), parseDoubleParameters(data["parameters"])));
	return actions;
}

ActionSchema::cptr Loader::loadActionSchema(const rapidjson::Value& node) {
	const std::string& name = node["name"].GetString();
	const Signature signature = parseStringList(node["signature"]);
	const std::vector<std::string> parameters = parseStringList(node["parameters"]);
	return new ActionSchema(name, signature, parameters);
	
}


AtomRange loadVariablesRanges(const VariableIdx var, const rapidjson::Value& data) {
  assert(data.IsArray());
  std::unique_ptr<AtomRange> ar(new AtomRange(var, std::make_pair(data[0].GetDouble(), data[1].GetDouble())));
  return *ar;
}

//This will be refactored. Now testing only
std::vector<AtomRange> Loader::loadGoalConditions(const rapidjson::Value& data) {
	std::vector<AtomRange> ranges;
	std::string model = data["robot"].GetString();
	ranges.push_back(loadVariablesRanges(0,data["x"]));
	ranges.push_back(loadVariablesRanges(1,data["y"]));
	ranges.push_back(loadVariablesRanges(2,data["yaw"]));
	return ranges;
}
/*
Goals Loader::loadGoalConditions(const rapidjson::Value& data) {
	std::vector<AtomRange> ranges;
	Goals goals;
	std::string model = data["robot"].GetString();
	ranges.push_back(loadVariablesRanges(0,data["x"]));
	ranges.push_back(loadVariablesRanges(1,data["y"]));
	ranges.push_back(loadVariablesRanges(2,data["yaw"]));
	return goals;
}
*/


rapidjson::Document Loader::loadJSONObject(const std::string& filename) {
	// Load and parse the JSON data file.
	std::ifstream in(filename);
	if (in.fail()) throw std::runtime_error("Could not open filename '" + filename + "'");
	std::string str((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());
	rapidjson::Document data;
	data.Parse(str.c_str());
	return data;
}




template<typename T>
std::vector<T> Loader::parseNumberList(const rapidjson::Value& data) {
	std::vector<T> output;
	for (unsigned i = 0; i < data.Size(); ++i) {
		output.push_back(boost::lexical_cast<T>(data[i].GetInt()));
	}
	return output;
}

std::vector<std::string> Loader::parseStringList(const rapidjson::Value& data) {
	std::vector<std::string> output;
	assert(data.IsString());
	for (unsigned i = 0; i < data.Size(); ++i) {
		output.push_back(data[i].GetString());
	}
	return output;
}


std::vector<float> Loader::parseDoubleParameters(const rapidjson::Value& data) {  
  std::vector<float> output;
  assert(data.IsArray());
  for (unsigned i = 0; i < data.Size(); ++i) 
    output.push_back(data[i].GetDouble());
  
  return output;
  
}

template<typename T>
std::vector<std::vector<T>> Loader::parseDoubleNumberList(const rapidjson::Value& data) {
	std::vector<std::vector<T>> output;
	assert(data.IsArray());
	if (data.Size() == 0) {
		output.push_back(std::vector<T>());
	} else {
		for (unsigned i = 0; i < data.Size(); ++i) {
			output.push_back(parseNumberList<T>(data[i]));
		}
	}
	return output;

}

} // namespaces
