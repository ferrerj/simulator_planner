#pragma once

#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <math.h>


class System {
  
  
public:
  
  static std::string get_home_folder() {
        struct passwd *pw = getpwuid(getuid());
    const char *homedir = pw->pw_dir;
  return std::string(homedir);
  }
  
  
};

namespace fs0 {

template< typename T, int nDimensions = 2 >
class Point
{
private:
    std::array< T, nDimensions > elements_;

public:
    typedef T ValueType;

    T& operator[]( int const i ) { return elements_[i];}

    T const& operator[]( int const i ) const { return elements_[i]; }

    void operator+=( Point const& other ) {
        for( int i = 0; i < nDimensions; ++i )
    elements_[i] += other.elements_[i];
    }

    void operator-=( Point const& other ) {
        for( int i = 0; i < nDimensions; ++i )
    elements_[i] -= other.elements_[i];
    }

    friend Point operator+( Point const& a, Point const& b ) {
        Point ret( a );
        ret += b;
        return ret;
    }

    friend Point operator-( Point const&a, Point const& b ) {
        Point ret( a );
        ret -= b;
        return ret;
    }

    Point(): elements_() {}

    Point( double x, double y ) {
        elements_[0] = x;
        elements_[1] = y;
    }

    Point( double x, double y, double z ) {
        elements_[0] = x;
        elements_[1] = y;
        elements_[2] = z;
    }
};

  typedef Point< double, 2 > Point2D;
  typedef Point< double, 3 > Point3D;


class Geometry {
  
public:
  


 static double distance(Point2D a, Point2D b ) {
   double diffX = b[0] - a[0];
   double diffY = b[1] - a[1];
   return sqrt( (diffX*diffX) + (diffY*diffY));
 }
 
 static double distance(Point3D a, Point3D b) {
  
   double diffX = b[0] - a[0];
   double diffY = b[1] - a[1];
   double diffZ = b[2] - a[2];
   return sqrt( (diffX*diffX) + (diffY*diffY) + (diffZ*diffZ)); }

  static double angle_between(Point2D A, Point2D B, Point2D C) {
    
   Point2D ab(B[0] - A[0], B[1] - A[1]);
   Point2D ac(C[0] - A[0], C[1] - A[1]);
   double dot = (ab[0] * ac[0] + ab[1] * ac[1]); // dot product
   double cross = (ab[0] * ac[1] - ab[1] * ac[0]); // cross product
   double angle_r = atan2(cross, dot);
  return  angle_r;
    
}

  static Point2D between_points(Point2D& A, Point2D& B) {
 
  Point2D mid((A[0]+B[0])/2, (A[1]+B[1])/2);
  return mid;
  
}

/*
  static double angle_between(Point2D A, Point2D B, Point2D C) {
    
return (atan2(B[1]-A[1], B[0]-A[0]) - atan2(C[1]-A[1], C[0]-A[0]))*-1;

    
}

  */ 

 
};

}