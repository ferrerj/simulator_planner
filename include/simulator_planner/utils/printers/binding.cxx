
#include <utils/printers/binding.hxx>
#include <problem.hxx>

namespace fs0 { namespace print {


std::ostream& binding::print(std::ostream& os) const {
	for (unsigned i = 0; i < _binding.size(); ++i) {
		os << _binding[i];
		if (i < _binding.size() - 1) os << ", ";
	}
	return os;
}

std::ostream& signature::print(std::ostream& os) const {
	for (unsigned i = 0; i < _parameter_names.size(); ++i) {
		os << _parameter_names[i] << ": " << _signature[i];
		if (i < _parameter_names.size() - 1) os << ", ";
	}
	return os;
}



} } // namespaces
