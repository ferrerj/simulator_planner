
#include <cassert>
#include <memory>
#include <utils/printers/printers.hxx>
#include <problem.hxx>

namespace fs0 {

void PlanPrinter::printPlan(const std::vector<GroundAction::IdType>& plan, const Problem& problem, std::ostream& out) {
	for (auto action:plan) {
		out << problem.get_action_name(action) << std::endl;
	}
}

void PlanPrinter::printPlanJSON(const std::vector<GroundAction::IdType>& plan, const Problem& problem, std::ostream& out) {
	out << "[";
	for ( unsigned k = 0; k < plan.size(); k++ ) {
		out << "\"" << problem.get_action_name(plan[k]) << "\"";
		if ( k < plan.size() - 1 ) out << ", ";
	}
	out << "]";
}

std::ostream& PlanPrinter::print(std::ostream& os) const {
	printPlan(_plan, Problem::getInstance(), os);
	return os;
}


} // namespaces
