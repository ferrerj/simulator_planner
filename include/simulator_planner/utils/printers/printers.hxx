
#pragma once

#include <vector>
#include <ostream>
#include <actions/ground_action.hxx>
#include <boost/units/detail/utility.hpp>

namespace fs0 {

class Problem;
class SupportedAction;

//! Helper class - debugging purposes only
class PlanPrinter {
protected:
	const ActionPlan& _plan;
public:
	PlanPrinter(const ActionPlan& plan) : _plan(plan) {}
	
	//! Prints a representation of the state to the given stream.
	friend std::ostream& operator<<(std::ostream &os, const PlanPrinter& o) { return o.print(os); }
	std::ostream& print(std::ostream& os) const;
	
	//! static helpers
	static void printPlan(const std::vector<GroundAction::IdType>& plan, const Problem& problem, std::ostream& out);
	static void printPlanJSON(const std::vector<GroundAction::IdType>& plan, const Problem& problem, std::ostream& out);	
};


} // namespaces
