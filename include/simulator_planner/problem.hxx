
#pragma once

#include <iosfwd>

#include <types.hxx>
#include <state.hxx>
#include <actions/ground_action.hxx>
#include <atom_range.hxx>

namespace fs0 {

class Problem {
public:
	//! Constructs a problem by loading the problem data from the given directory.
	Problem(const std::string& domain, const std::string& instance, const std::vector<ActionSchema::cptr>& schemata, const std::vector<GroundAction::cptr>& grounded, const State& init, const std::vector<AtomRange>& goal_conditions)
		: _domain(domain), _problem_instance(instance), _schemata(schemata), _ground(grounded), _init(init), _goal_conditions(goal_conditions)
	{}
	~Problem();

	const State& getInitialState() const { return _init; }

	
	const std::vector<GroundAction::cptr>& getGroundActions() const { return _ground; }
	//const void addGroundActions(GroundAction::cptr action) const { _ground.push_back(action); }
	
	std::string get_action_name(unsigned action) const;


	static void setInstance(std::unique_ptr<Problem>&& problem) {
		if (_instance) throw std::runtime_error("Problem instance has already been set");
		_instance = std::move(problem);
		_instance->bootstrap();
	}
	
	//! const version of the singleton accessor
	static const Problem& getInstance() {
		if (!_instance) throw std::runtime_error("Problem has not been instantiated yet");
		return *_instance;
	}
	
	//! This performs a number of necessary routines once all of the problem information has been defined.
	void bootstrap();
	
	const std::string& getDomainName() const { return _domain; }
	const std::string& getInstanceName() const { return _problem_instance; }
	
	const std::vector<AtomRange>& getGoalConditions() const { return _goal_conditions;}
	//Goals& getGoalConditions() const { return _goal_conditions;}

	
	//! Prints a representation of the object to the given stream.
	friend std::ostream& operator<<(std::ostream &os, const Problem& o) { return o.print(os); }
	std::ostream& print(std::ostream& os) const;

protected:
	//! The names of the problem domain and instance
	std::string _domain;
	std::string _problem_instance;

	// The set of action schemata
	std::vector<ActionSchema::cptr> _schemata;
	
	// The set of grounded actions of the problem
	std::vector<GroundAction::cptr> _ground;

	//! The initial state of the problem
	State _init;
	
	std::vector<AtomRange> _goal_conditions;
	//mutable Goals _goal_conditions;
	
	//! The singleton instance
	static std::unique_ptr<Problem> _instance;
};

} // namespaces
