
#pragma once

//#include <search/options.hxx>
#include <lib/rapidjson/document.h>

namespace fs0 { class Problem; class Simulator; }

namespace fs0 { namespace engines {
	
//! A basic runner script to bootstrap the problem and run the search engine
class Runner {
public:
	//! Set up the runner, loading the problem, the configuration, etc.
	//Runner(const Simulator& simulator, const EngineOptions& options);
	Runner(const Simulator& simulator);
	
	//! Run the search engine
	int run();

protected:
	const Simulator& _simulator;
	
	//! The command-line options for this run
	//const EngineOptions _options;
};

} } // namespaces
