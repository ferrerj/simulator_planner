
#include <problem.hxx>
#include <utils/loader.hxx>
#include <search/search.hxx>

#include <search/runner.hxx>
#include <utils/config.hxx>

#include <simulator/simulator.hxx>


namespace fs0 { namespace engines {

  /*
Runner::Runner(const Simulator& simulator, const EngineOptions& options) 
	: _simulator(simulator), _options(options)
{
	Logger::init("./logs");
	Config::init("config.json");

	FINFO("main", "Generating the problem (" << _options.getDataDir() << ")... ");
	auto data = Loader::loadJSONObject(_options.getDataDir() + "/problem.json");
	auto init = _simulator.get_current_state(); // The initial state is the current state of the simulator
	Problem::setInstance(Loader::loadProblem(data, std::move(init)));
}
*/


Runner::Runner(const Simulator& simulator) 
	: _simulator(simulator)
{
	//Logger::init("./logs");
	Config::init("config.json");

	//FINFO("main", "Generating the problem (" << _options.getDataDir() << ")... ");
	//auto data = Loader::loadJSONObject("/problem.json");
	//auto init = _simulator.get_current_state(); // The initial state is the current state of the simulator
	//Problem::setInstance(Loader::loadProblem(data, std::move(init)));
}

int Runner::run() {
	const Problem& problem = Problem::getInstance();
	const Config& config = Config::instance();
	
	//FINFO("main", "Problem instance loaded:" << std::endl << problem);
	SearchUtils::report_stats(problem);
	
	//FINFO("main", "Planner configuration: " << std::endl << config);
	SimulatorStateModel model(problem, _simulator);
	SearchUtils::instantiate_search_engine_and_run(model, config, 1000, "data/");
	return 0;
}

} } // namespaces
