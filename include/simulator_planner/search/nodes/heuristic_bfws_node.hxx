#pragma once

//#include <utils/logging.hxx>
#include <actions/ground_action.hxx>
#include <heuristics/novelty/fs0_novelty_evaluator.hxx>

namespace fs0 { namespace engines {
  
  
  class HeuristicEnsemble {
public:

	const	SimulatorStateModel&							_problem;
	std::vector< GenericNoveltyEvaluator >						_novelty_heuristic;
	unsigned									_max_novelty;

	HeuristicEnsemble( const SimulatorStateModel& problem ): 
		_problem( problem ), _max_novelty(0)
 		{		 
		_novelty_heuristic.resize( problem.getProblem().getGoalConditions().size() + 1 );
		//std::cout << "# Novelty evaluators: " << _novelty_heuristic.size() << std::endl;

	}

	~HeuristicEnsemble() {
		for (unsigned j = 0; j < _novelty_heuristic.size(); j++)
			for ( unsigned k = 1; k <= novelty_bound(); k++ ) {
				//std::cout << "# novelty(s)[#goals=" << j << "]=" << k << " : " << _novelty_heuristic[j].get_num_states(k) << std::endl;;
			}
	}

	void setup( int max_novelty ) {
		_max_novelty = max_novelty;
		for ( unsigned k = 0; k < _novelty_heuristic.size(); k++ ) {
			_novelty_heuristic[k].set_max_novelty( novelty_bound() );
			_novelty_heuristic[k].selectFeatures( _problem.getProblem());
		}
	}

	//NOT USED
	/*
	float	evaluate_reachability( const GenericState& s ) {
		return _reachability_heuristic.evaluate( s );
	}
	*/
	
	//TODO: heurisitc which depensd on the distance from Ci to Cg.
	float metric_heuristic(const State& s) {
	  Point2D p(s[0], s[1]);
	  Point2D q(-1, 3);//Just to test
	  return Geometry::distance(p,q);
	}

	unsigned evaluate_novelty( const State& s ) {
		return _novelty_heuristic[evaluate_num_unsat_goals(s)].evaluate( s );
	}

	float evaluate_num_unsat_goals(const State& s) { 
		unsigned unsatisfied = 0;
		for (auto range_condition:_problem.getProblem().getGoalConditions()) {
			if (!s.satisfies(range_condition)) ++unsatisfied;
		}
		return unsatisfied;
	}
	
	
	unsigned	novelty_bound() { return _max_novelty; }


};
  
  
  
template <typename State>
class HeuristicBFWSNode {
public:
	State state;
	GroundAction::IdType action;
	std::shared_ptr<HeuristicBFWSNode<State> > parent;
	unsigned g; //Accumulated cost
	unsigned novelty; // evaluation function
	bool _is_dead_end;
	unsigned num_unsat;
	
	public:
	// Kill default constructors
	explicit HeuristicBFWSNode();
	
	//! Constructor with full copying of the state (expensive)
	HeuristicBFWSNode(const State& s)
		: state(s), action( GroundAction::invalid_action_id ), parent( nullptr ), novelty(0), g(0), _is_dead_end(false), num_unsat(0) {
	}
	
	//! Constructor with move of the state (cheaper)
	HeuristicBFWSNode(State&& _state, GroundAction::IdType _action, std::shared_ptr< HeuristicBFWSNode<State> > _parent) :
		state(_state), action(_action), parent(_parent), novelty(0), g(_parent->g + 1), _is_dead_end(false), num_unsat(0) {
	}

	virtual ~HeuristicBFWSNode() {}

	bool has_parent() const { return parent != nullptr; }

	void print( std::ostream& os ) const {
		os << "{@ = " << this << ", s = " << state << ", novelty = " << novelty << ", g = " << g << " unsat = " << num_unsat << ", parent = " << parent << "}\n";
	}
	

	bool operator==( const HeuristicBFWSNode<State>& o ) const { return state == o.state; }
	
	
	// MRJ: This is part of the required interface of the Heuristic
	template <typename Heuristic>
	void	evaluate_with( Heuristic& heuristic ) {
		heuristic.setup(2);
		novelty = heuristic.evaluate_novelty( state );
		_is_dead_end = novelty > heuristic.novelty_bound();
		num_unsat = heuristic.evaluate_num_unsat_goals( state );
		//print(std::cout);
		//novelty = novelty + heuristic.metric_heuristic(state);
		//g=g+heuristic.metric_heuristic(state);
		
		/* NOT USED as this is the heuristic function for gbfs(f) Kind of...
		h = heuristic.evaluate_reachability( state );
		unsigned ha = 2;
		if ( parent != nullptr && h < parent->h ) ha = 1;
		novelty = 2 * (novelty - 1) + ha;
		*/
	}

	bool dead_end() const { return _is_dead_end; }

	std::size_t hash() const { return state.hash(); }

	// MRJ: With this we implement Greedy Best First modified to be aware of state novelty
	bool operator>( const HeuristicBFWSNode<State>& other ) const {
		if ( novelty > other.novelty ) return true;
		if ( novelty < other.novelty ) return false;
		if ( num_unsat > other.num_unsat ) return true;
		if ( num_unsat < other.num_unsat ) return true;
		return g > other.g;
	}
	

	
};
  
  
  


}}