#pragma once

#include <functional>

//#include <search/nodes/blind_search_node.hxx>
#include <search/nodes/heuristic_bfws_node.hxx>

#include <search/components/single_novelty.hxx>
#include <search/engines/registry.hxx>
#include <state_model.hxx>

#include <aptk2/search/algorithms/generic_search.hxx>
//#include <aptk2/search/algorithms/breadth_first_search.hxx>
#include <aptk2/search/algorithms/best_first_search.hxx>
#include <aptk2/search/components/unsorted_open_list_impl.hxx>
#include <aptk2/search/components/closed_list_impl.hxx>
#include <aptk2/search/components/stl_unsorted_fifo_open_list.hxx>
#include <aptk2/search/components/stl_sorted_open_list.hxx>
#include <aptk2/search/components/sorted_open_list_impl.hxx>
#include <search/components/unsat_goals_novelty.hxx>

namespace fs0 { namespace engines {

class BFWSAlgorithm : public FS0SearchAlgorithm {
public:
	//! IW uses a simple blind-search node
	typedef HeuristicBFWSNode<State> SearchNode;
	
	//! IW uses a single novelty component as the open list evaluator
	//typedef SingleNoveltyComponent<SearchNode> SearchNoveltyEvaluator;
	
	//typedef UnsatGoalsNoveltyComponent<SearchNode> NoveltyHeuristic;
	
	//! IW uses an unsorted queue with a NoveltyEvaluator acceptor
	//typedef aptk::StlUnsortedFIFO<SearchNode, SearchNoveltyEvaluator> OpenList;
	//typedef aptk::StlSortedOpenList<SearchNode, SearchNoveltyEvaluator> OpenList;
		
	
	//! The base algorithm for IW is a simple Breadth-First Search
	//typedef aptk::StlBreadthFirstSearch<SearchNode, SimulatorStateModel, OpenList> BaseAlgorithm;
	 // typedef aptk::StlBestFirstSearch<SearchNode, SimulatorStateModel, OpenList> BaseAlgorithm;
	 //typedef aptk::StlBestFirstSearch<SearchNode, NoveltyHeuristic, SimulatorStateModel > BaseAlgorithm;
	  typedef aptk::StlBestFirstSearch<SearchNode, HeuristicEnsemble, SimulatorStateModel > BaseAlgorithm;

	
	BFWSAlgorithm(const SimulatorStateModel& model, unsigned initial_max_width, unsigned final_max_width)
		: FS0SearchAlgorithm(model), _algorithm(nullptr), _current_max_width(initial_max_width), _final_max_width(final_max_width)
	{
	}
	
	virtual ~BFWSAlgorithm() {
		if (_algorithm) delete _algorithm;
	}
	
	
	virtual bool search(const State& state, typename FS0SearchAlgorithm::Plan& solution) {
                //std::cout << state << std::endl;
		setup_brfs_algorithm(_current_max_width);
		this->generated =5;
		while(_current_max_width <= _final_max_width) {
			if(_algorithm->search(state, solution)) {
			 this->generated = _algorithm->generated;
			 this->expanded = _algorithm->expanded;//Check this
			  return true; 
			}
			++_current_max_width;
			setup_brfs_algorithm(_current_max_width);

			solution.clear();
		}
		return false;
	}
	
	void setup_brfs_algorithm(unsigned max_width) {
		if (_algorithm) delete _algorithm;
		//std::shared_ptr<SearchNoveltyEvaluator> evaluator = std::make_shared<SearchNoveltyEvaluator>(this->model, max_width);
		//std::shared_ptr<NoveltyHeuristic> evaluator = std::make_shared<NoveltyHeuristic>(this->model, max_width);
		//NoveltyHeuristic evaluator(model, _final_max_width);
		HeuristicEnsemble evaluator(model);
		//evaluator.setup(max_width);
		_algorithm = new BaseAlgorithm(model);
		//_algorithm = new BaseAlgorithm(model, evaluator);
		//aptk::StlBestFirstSearch<SearchNode, HeuristicEnsemble, SimulatorStateModel > BaseAlgorithm(model);
		//BaseAlgorithm.heuristic_function.setup(2);


	}

	
protected:
	
	//!
	SearchAlgorithm* _algorithm;
	//!
	unsigned _current_max_width;
	//!
	unsigned _final_max_width;
};

} } // namespaces
