

#include <aptk2/tools/resources_control.hxx>

#include <problem.hxx>
#include <search/search.hxx>
#include <search/engines/registry.hxx>
#include <utils/printers/printers.hxx>
#include <utils/config.hxx>
//#include <utils/logging.hxx>
#include <fstream>

namespace fs0 { namespace engines {

float SearchUtils::do_search(fs0::engines::FS0SearchAlgorithm& engine, const Problem& problem, const Config& config, const std::string& out_dir) {

	std::ofstream out(out_dir + "/searchlog.out");
	std::ofstream plan_out(out_dir + "/first.plan");
	std::ofstream json_out( out_dir + "/results.json" );

	std::cout << "Writing results to " << out_dir + "/searchlog.out" << std::endl;

	std::vector<GroundAction::IdType> plan;
	float t0 = aptk::time_used();
	bool solved = engine.solve_model( plan );
	float total_time = aptk::time_used() - t0;

	if ( solved ) {
		PlanPrinter::printPlan(plan, problem, out);
		PlanPrinter::printPlan(plan, problem, plan_out);
	}

	out << "Plan length : " << plan.size() << std::endl;
	out << "Total time: " << total_time << std::endl;
	out << "Nodes generated during search: " << engine.generated << std::endl;
	out << "Nodes expanded during search: " << engine.expanded << std::endl;

	std::string eval_speed = (total_time > 0) ? std::to_string((float) engine.generated / total_time) : "-";
	out << "Heuristic evaluations per second: " <<  eval_speed << std::endl;
	out << "IW(" << config.getOption<int>("engine.max_novelty") << ")" << std::endl;
	out << "Initial width: " << config.getOption<int>("engine.initial_width") << std::endl;
	out << "Delta Time(s): " << config.getOption<float>("engine.deltaT") << std::endl;
	out << "State var discretization(for roundup): " << config.getOption<int>("engine.discretization") << std::endl;
	out << "Translation velocity(m/s): " << config.getOption<int>("engine.v_trans") << std::endl;
	out << "Rotation velocity(rad/s): " << config.getOption<float>("engine.v_rot") << std::endl;
	

	out.close();
	plan_out.close();

	json_out << "{" << std::endl;
	json_out << "\tsearch_time : " << total_time << "," << std::endl;
	json_out << "\tgenerated : " << engine.generated << "," << std::endl;
	json_out << "\texpanded : " << engine.expanded << "," << std::endl;
	json_out << "\teval_per_second : " << eval_speed << "," << std::endl;
	json_out << "\tsolved : " << ( solved ? "true" : "false" ) << "," << std::endl;
	json_out << "\tplan_length : " << plan.size() << "," << std::endl;
	json_out << "\tplan : ";
	if ( solved )
		PlanPrinter::printPlanJSON( plan, problem, json_out);
	else
		json_out << "null";
	json_out << std::endl;
	json_out << "}" << std::endl;

	json_out.close();

	return total_time;
}

void SearchUtils::instantiate_search_engine_and_run(const SimulatorStateModel& model, const Config& config, int timeout, const std::string& out_dir) {
	float timer = 0.0;
	std::cout << "Starting search (time budget is " << timeout << " secs)..." << std::endl;
	auto creator = fs0::engines::EngineRegistry::instance().get(config.getEngineTag());
	auto engine = creator->create(config, model);
	timer = do_search(*engine, model.getProblem(), config, out_dir);
	std::cout << "Search completed in " << timer << " secs" << std::endl;
}

void SearchUtils::report_stats(const Problem& problem) {
	auto actions = problem.getGroundActions();
	unsigned n_actions = actions.size();
	
	std::cout << "Number of state variables: " << problem.getInitialState().size() << std::endl;

	std::cout << "Number of ground actions: " << n_actions << std::endl;
	if (n_actions > 1000) {
		std::cout << "WARNING: The number of ground actions (" << n_actions <<
		") is too high for our current applicable action strategy to perform well." << std::endl;
	}

	std::cout << "Number of goal conditions: " << problem.getGoalConditions().size() << std::endl;
}

} } // namespaces
