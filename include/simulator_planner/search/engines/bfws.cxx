#include <search/engines/bfws.hxx>
//#include <search/algorithms/iterated_width.hxx>
#include <search/algorithms/bfws_algorithm.hxx>
#include <state_model.hxx>
#include <utils/config.hxx>
#include <aptk2/search/algorithms/breadth_first_search.hxx>
#include <aptk2/search/algorithms/best_first_search.hxx>

namespace fs0 { namespace engines {
	

	
std::unique_ptr<FS0SearchAlgorithm> BFWSEngineCreator::create(const Config& config, const SimulatorStateModel& model) const {
	
	unsigned max_novelty = config.getOption<int>("engine.max_novelty");
	unsigned initial_width = config.getOption<int>("engine.initial_width");
	
	std::cout << "Max novelty: " << max_novelty << std::endl;
	std::cout << "Initial Width: " << initial_width << std::endl;
	std::cout << "State vars of model: " << model.getProblem().getInitialState().size() << std::endl;


	FS0SearchAlgorithm* engine = new BFWSAlgorithm(model, initial_width, max_novelty);
	return std::unique_ptr<FS0SearchAlgorithm>(engine);
}

} } // namespaces
