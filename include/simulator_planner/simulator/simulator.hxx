
#pragma once

#include <state.hxx>

namespace fs0 { 

class State; class GroundAction;

class Simulator {
public:
	Simulator() {}
	virtual ~Simulator() {}
	
	//! Executes a given action in the simulator and returns the resulting state
	virtual State execute(const State& state, const GroundAction& a) const = 0;
	
	//! Return the current state of the simulator (TODO: Is this really needed?)
	//virtual State get_current_state() const = 0;
	
};

} // namespaces
