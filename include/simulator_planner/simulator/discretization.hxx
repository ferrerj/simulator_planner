
#pragma once

#include <iosfwd>

#include <types.hxx>
#include <state.hxx>
#include <actions/ground_action.hxx>
#include <utils/config.hxx>
#include <utils/utils.hxx>



namespace fs0 { 

class Discretizer {
public:
	Discretizer() {
	  std::string home = System::get_home_folder();
	}
	//This discretize method is used to discretize the state variables in order to check the novelty of a state.
	//If discretize= 1000 and x = 3.56347 then the novelty is calculated based on x = 3.563
	int discretize(VariableIdx variable, float value) const { 
	    const fs0::Config& _config = fs0::Config::instance();
	    unsigned discretization = _config.getOption<int>("engine.discretization");
	    double r_value = round(value*discretization)/discretization;
	    //std::cout << "Discretized value: " << floor(r_value*10) << std::endl;
	    return floor(r_value*10);
	}
	
	//Discretize the value due a resolution factor
	//For distance and time
	static float resolution(float value) {
	  const fs0::Config& _config = fs0::Config::instance();
	  unsigned resolution = _config.getOption<int>("engine.resolution");
	  return value/resolution;
	}
	
protected:
	
};

} // namespaces
