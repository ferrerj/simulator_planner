#pragma once

#include <utils/config.hxx>


class Robot {
  
  typedef std::pair<double, double> double_range;
  typedef std::vector<std::string> strVec;
  
private:
  
  std::string _robot;
  std::string _base;
  std::string _gripper;
  std::string _hand;
  double_range _grasp_dist;
  strVec _grasping_links;
  strVec _joints;
  
public:
  
  Robot(std::string robot, std::string base, std::string gripper, std::string hand, double_range grasp_dist, 
	strVec grasping_links, strVec joints):
	  _robot(robot), _base(base), _gripper(gripper), _hand(hand), _grasp_dist(grasp_dist),
	  _grasping_links(grasping_links), _joints(joints)
  {
    
  }
  
  Robot(const fs0::Config& config){
    setup(config);
    
  }
  

  
  std::string& robot() {return _robot;}
  std::string& base() {return _base;}
  std::string& gripper() {return _gripper;}
  std::string& hand() {return _hand;}
  double_range& grasp_dist() {return _grasp_dist;}
  strVec& grasping_links() {return _grasping_links;}
  strVec& joints() {return _joints;}
  
  
  //Load specifications from a json file
  void setup(const fs0::Config& config) {
 
    _robot = config.getOption<std::string>("robot.robot");
    _base = config.getOption<std::string>("robot.base");
    _gripper = config.getOption<std::string>("robot.gripper");
    _hand = config.getOption<std::string>("robot.hand");
    _grasp_dist = config.as_pair<double>("robot.grasping_eval_dist");
    _grasping_links = config.as_vector<std::string>("robot.grasping_links");
    _joints = config.as_vector<std::string>("robot.arm_joints");

  }
  
  void print() {
   
    std::cout << "Robot: " << _robot << ", Base: " << _base << ", Gripper: " << _gripper << ", Hand: " << _hand <<
    ", Grasping dist[min, max]: [" << _grasp_dist.first << ", " << _grasp_dist.second << "], Grasping links: ";
    for(auto& it: _grasping_links)
      std::cout << it << ", ";
    std::cout << "Joints: ";
    for(auto& it: _joints)
      std::cout << it << ", ";
    std::cout << std::endl;
    
  }
  
  
  
  
  
};