#pragma once

#include <state.hxx>
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/Float64.h>
#include "dynamic_joint/attached.h"
#include "dynamic_joint/attach.h"
#include "gazebo_msgs/ContactsState.h"

namespace fs0 {

class State; class GroundAction;


class ActionKomodo {
  
private:
  
  ros::NodeHandle _n;
  ros::Publisher _cmd_vel_pub;
  ros::Publisher _base_ctrl_pub;
  ros::Publisher _elbow1_ctrl_pub;
  ros::Publisher _elbow2_ctrl_pub;
  ros::Publisher _shoulder_ctrl_pub;
  ros::Publisher _wrist_ctrl_pub;
  ros::Publisher _rfinger_ctrl_pub;
  ros::Publisher _lfinger_ctrl_pub;

 
public:
  
  ActionKomodo() {}
  
  ActionKomodo(ros::NodeHandle n): _n(n) {
    
    _cmd_vel_pub = n.advertise<geometry_msgs::Twist>("/a_komodo/diff_driver/command", 10);
    _base_ctrl_pub = n.advertise<std_msgs::Float64>("/a_komodo/base_rotation_controller/command", 10);
    _elbow1_ctrl_pub = n.advertise<std_msgs::Float64>("/a_komodo/elbow1_controller/command", 10);
    _elbow2_ctrl_pub = n.advertise<std_msgs::Float64>("/a_komodo/elbow2_controller/command", 10);
    _shoulder_ctrl_pub = n.advertise<std_msgs::Float64>("/a_komodo/shoulder_controller/command", 10);
    _wrist_ctrl_pub = n.advertise<std_msgs::Float64>("/a_komodo/wrist_controller/command", 10);
    _rfinger_ctrl_pub = n.advertise<std_msgs::Float64>("/a_komodo/right_finger_controller/command", 10);
    _lfinger_ctrl_pub = n.advertise<std_msgs::Float64>("/a_komodo/left_finger_controller/command", 10);
 
  }

  
   ~ActionKomodo() {}
  
   void cmd_vel_action(const GroundAction& a, float v_trans, float v_rot, float deltaT) {
    geometry_msgs::Twist cmd_vel_msg;
    cmd_vel_msg.linear.x = a.getBinding().at(0)*v_trans;
    cmd_vel_msg.linear.y = a.getBinding().at(1)*v_trans;
    cmd_vel_msg.angular.z = a.getBinding().at(2)*v_rot;
    _cmd_vel_pub.publish(cmd_vel_msg);
    ros::WallDuration(deltaT).sleep();
    stop_cmd_vel_action();
    usleep(10000);
  }
  
  void stop_cmd_vel_action() { 
    geometry_msgs::Twist cmd_vel_msg;
    cmd_vel_msg.linear.x = 0.0;
    cmd_vel_msg.linear.y = 0.0;
    cmd_vel_msg.angular.z = 0.0;
    _cmd_vel_pub.publish(cmd_vel_msg);
    usleep(10000);
    
 }
 
 
 //By ground action
  void base_rotation_controller(const GroundAction& a, float factor) {
    std_msgs::Float64 msg;
    msg.data = a.getBinding().at(0)*factor;
    _base_ctrl_pub.publish(msg);
    usleep(100000);
 }
 
   void elbow1_controller(const GroundAction& a, float factor) {
    std_msgs::Float64 msg;
    msg.data = a.getBinding().at(0)*factor;
    _elbow1_ctrl_pub.publish(msg);
    usleep(100000);
 }
 
   void elbow2_controller(const GroundAction& a, float factor) {
    std_msgs::Float64 msg;
    msg.data = a.getBinding().at(0)*factor;
    _elbow2_ctrl_pub.publish(msg);
    usleep(100000);
 }
 
   void shoulder_controller(const GroundAction& a, float factor) {
    std_msgs::Float64 msg;
    msg.data = a.getBinding().at(0)*factor;
    _shoulder_ctrl_pub.publish(msg);
    usleep(100000);
 }
 
   void wrist_controller(const GroundAction& a, float factor) {
    std_msgs::Float64 msg;
    msg.data = a.getBinding().at(0)*factor;
    _wrist_ctrl_pub.publish(msg);
    usleep(100000);
 }
 
 //By value
 
   void base_rotation_controller(float v, float factor) {
    std_msgs::Float64 msg;
    msg.data = v*factor;
    _base_ctrl_pub.publish(msg);
    usleep(100000);
 }
 
   void elbow1_controller(float v, float factor) {
    std_msgs::Float64 msg;
    msg.data =v*factor;
    _elbow1_ctrl_pub.publish(msg);
    usleep(10000);
 }
 
   void elbow2_controller(float v, float factor) {
    std_msgs::Float64 msg;
    msg.data = v*factor;
    _elbow2_ctrl_pub.publish(msg);
    usleep(10000);
 }
 
   void shoulder_controller(float v, float factor) {
    std_msgs::Float64 msg;
    msg.data = v*factor;
    _shoulder_ctrl_pub.publish(msg);
    usleep(10000);
 }
 
   void wrist_controller(float v, float factor) {
    std_msgs::Float64 msg;
    msg.data = v*factor;
    _wrist_ctrl_pub.publish(msg);
    usleep(10000);
 }
 
 
 
 
 
 void open_gripper() {
  std_msgs::Float64 msg;
  msg.data = 1.0;
  _rfinger_ctrl_pub.publish(msg);
  _lfinger_ctrl_pub.publish(msg);
    
 }
 
  void close_gripper() {
  std_msgs::Float64 msg;
  msg.data = -1.0;
  _rfinger_ctrl_pub.publish(msg);
  _lfinger_ctrl_pub.publish(msg);
    
 }
 
  
  
  
};

}