#pragma once

#include <state.hxx>
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <trajectory_msgs/JointTrajectory.h>
#include "dynamic_joint/attached.h"
#include "dynamic_joint/attach.h"
#include "gazebo_msgs/ContactsState.h"
#include "gazebo_ros_link_attacher/Attach.h"

using namespace std;

namespace fs0 {

class State; class GroundAction;


class ActionSimulator {
  
  
public:
  
  typedef const std::string str;
  
  ActionSimulator() {}
  ~ActionSimulator() {}
  
  static void cmd_vel_action(const GroundAction& a, float v_trans, float v_rot, float deltaT, const ros::Publisher& cmd_vel_pub) {
      // std::cout << "MOVE" << std::endl;
    geometry_msgs::Twist cmd_vel_msg;
    cmd_vel_msg.linear.x = a.getBinding().at(0)*v_trans;
    cmd_vel_msg.linear.y = a.getBinding().at(1)*v_trans;
    cmd_vel_msg.angular.z = a.getBinding().at(2)*v_rot;
    cmd_vel_pub.publish(cmd_vel_msg);
    ros::WallDuration(deltaT).sleep();
    stop_cmd_vel_action(cmd_vel_pub);
    usleep(1000);
  }
  
  static void cmd_vel_simple_action(const int a, float v_trans, float v_rot, float time, const ros::Publisher& cmd_vel_pub) {

    double x, y, z = 0.0;
    
    if(a == 0) x = 1;
    else if(a == 1) z = 1;
    else if(a == 2) z = -1;
    
    geometry_msgs::Twist cmd_vel_msg;
    cmd_vel_msg.linear.x = x*v_trans;
    cmd_vel_msg.linear.y = y*v_trans;
    cmd_vel_msg.angular.z = z*v_rot;
    cmd_vel_pub.publish(cmd_vel_msg);
    ros::WallDuration(time).sleep();
    stop_cmd_vel_action(cmd_vel_pub);
    usleep(1000);
  }
  
  static void stop_cmd_vel_action(const ros::Publisher& cmd_vel_pub) { 
    geometry_msgs::Twist cmd_vel_msg;
    cmd_vel_msg.linear.x = 0.0;
    cmd_vel_msg.linear.y = 0.0;
    cmd_vel_msg.angular.z = 0.0;
    cmd_vel_pub.publish(cmd_vel_msg);
    usleep(1000);
    
 }
 

 static void arm_command(const GroundAction& a, std::vector<std::string>& joints, const ros::Publisher& joint_traj_pub) {
   trajectory_msgs::JointTrajectory command;
   command.points.resize(1);
   command.points[0].positions.resize(joints.size());
   command.points[0].velocities.resize(joints.size());

   for(unsigned i = 0; i <= joints.size(); i++) {
    command.joint_names.push_back(joints.at(i));
    std::cout << "joints: " << joints.at(i) << std::endl;
    command.points[0].positions[i] = 0.0;
    command.points[0].velocities[i] = 0.0;
    command.points[0].time_from_start = ros::Duration(1.0);
   }
   joint_traj_pub.publish(command);
 }
 
 
 
 static void grasp(str& robot, str& lRobot, str& obj, str& lObj, ros::ServiceClient& attach_pub) {
  dynamic_joint::attach attach;
  attach.request.model1 = robot;
  attach.request.link1 = lRobot;
  attach.request.model2 = obj;
  attach.request.link2 = lObj;
  attach.request.grasp = true;
  if(attach_pub.call(attach))
    attach.response.success = true;
  
   usleep(1000);

   
 }
 
 static void place(ros::ServiceClient& attach_pub) {
 dynamic_joint::attach attach;
  attach.request.model1 = "";
  attach.request.link1 = "";
  attach.request.model2 = "";
  attach.request.link2 = "";
  attach.request.grasp = false;
  if(attach_pub.call(attach))
    attach.response.success = true;
  
   usleep(1000);

 }
 
 static void close_gripper(vector<string>& joints, ros:: NodeHandle& n) {
 set_model_configuration(0.0, 0.45, 0.05, joints, n); 
}

static void open_gripper(vector<string>& joints, ros:: NodeHandle& n) {
   set_model_configuration_open(0.4, 0.0, -0.05, joints, n); 
  
}


// Attach an object to the robot
 static void grasp_attaching(str& obj, ros::ServiceClient& attach_pub) {
   
   std::string link = obj+"::gl_";
   //std::string command = "rosservice call /attach \"{model1: 'a_robot_big', link1: 'a_robot_big::gripper_big_mov::palm', model2: '"+ obj +"', link2: '"+link+"', grasp: true}\"";
   //std::string command = "rosservice call /link_attacher_node/attach \"model_name_1: 'a_robot_big' link_name_1: 'a_robot_big::gripper_big_mov::palm' model_name_2: '"+obj+"' link_name_2: '"+link+"'\""; 
   //command = "rosservice call /link_attacher_node/attach \"model_name_1: 'a_robot_big' link_name_1: 'a_robot_big::gripper_big_mov::palm' model_name_2: 'o1' link_name_2: 'o1::gl_'\" ";
  // system(command.c_str());
   
   gazebo_ros_link_attacher::Attach attach;
   attach.request.model_name_1 = "a_robot_big";
   attach.request.link_name_1 = "a_robot_big::gripper_big::palm";
   attach.request.model_name_2 = obj;
   attach.request.link_name_2 = link;
   attach_pub.call(attach);
     

   usleep(1000);

 }

 
 //Dettach an object from the robot
 static void place_dettaching(str& obj, ros::ServiceClient& dettach_pub) {
      std::string link = obj+"::gl_";


   gazebo_ros_link_attacher::Attach attach;
   attach.request.model_name_1 = "a_robot_big";
   attach.request.link_name_1 = "a_robot_big::gripper_big::palm";
   attach.request.model_name_2 = obj;
   attach.request.link_name_2 = link;
  dettach_pub.call(attach);
  
   usleep(1000);

 }
 
 
 //Set a model to a state configuration
static bool set_model_configuration(double from, double to, double incr, vector<string>& joints, ros:: NodeHandle& n) {
   ros::ServiceClient client = n.serviceClient<gazebo_msgs::SetModelConfiguration>("/gazebo/set_model_configuration");
  gazebo_msgs::SetModelConfiguration setmodelconf;
  setmodelconf.request.model_name = "a_robot_big";
   
  while (from < to){
  
  for(unsigned i = 0; i < joints.size(); ++i) {
    setmodelconf.request.joint_names.push_back(joints.at(i));
   setmodelconf.request.joint_positions.push_back(from); 
  }
    client.call(setmodelconf);
      from = from+incr;
    
  }
  
  return true;
}

 //Set a model to a state configuration
static bool set_model_configuration_open(double from, double to, double incr, vector<string>& joints, ros:: NodeHandle& n) {
   ros::ServiceClient client = n.serviceClient<gazebo_msgs::SetModelConfiguration>("/gazebo/set_model_configuration");
  gazebo_msgs::SetModelConfiguration setmodelconf;
  setmodelconf.request.model_name = "a_robot_big";
   
  while(from > to) {
  
  for(unsigned i = 0; i < joints.size(); ++i) {
    setmodelconf.request.joint_names.push_back(joints.at(i));
   setmodelconf.request.joint_positions.push_back(from); 
  }
    client.call(setmodelconf);
      from = from+incr;
  }
    
  
  return true;
}
 

 

  
  
protected:
  
  
  
};

}