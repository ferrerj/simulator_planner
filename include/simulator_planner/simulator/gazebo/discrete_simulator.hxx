
#pragma once

#include <state.hxx>
#include <ros/ros.h>
#include <gazebo_msgs/GetModelState.h>
#include <gazebo_msgs/SetModelState.h>
#include <gazebo_msgs/GetWorldProperties.h>
#include <gazebo_msgs/GetModelProperties.h>
#include <tf/transform_datatypes.h>
#include <simulator/simulator.hxx>

namespace fs0 { 

class State; class GroundAction;

class DiscreteSimulator : public Simulator {
public:

	DiscreteSimulator() {}
	~DiscreteSimulator() {}

	typedef std::set<std::string> strSet;
	
//TODO: This execute is for move like actions. Grasp and Place??
State execute(const State& s0, const GroundAction& a) const {	
    State state(s0);
    set_sim_to_state(state);//Set the simulator to the given state
    VariableIdx modelIdx = a.getBinding().at(0)*3;
    for(unsigned i = 0; i < 3; ++i) {
      float value = state[modelIdx+i] + a.getBinding().at(i+1);
      state.set_value(modelIdx+i, value);
    }
    std::cout << "Applying action " << a << " to state " << s0 << " results in state " << state << std::endl;
    set_sim_to_state(state);//Set the simulator to the new state after apply an action
    return get_current_state(true);//Get the state after applying the acion
}

//Get all the model names  
void get_models_from_sim() {
    
  ros::ServiceClient client = n.serviceClient<gazebo_msgs::GetWorldProperties>("/gazebo/get_world_properties");
  gazebo_msgs::GetWorldProperties getworldproperties; 
   if(client.call(getworldproperties))
 	for(std::vector<std::string>::iterator mit = getworldproperties.response.model_names.begin(); 
	    mit != getworldproperties.response.model_names.end();mit++)
	    if(!isStatic(*mit))
		_modelsSet.insert(*mit);
  }

//Check is static or not
bool isStatic(std::string model) {
	 bool isstatic = false;
	 ros::ServiceClient client = n.serviceClient<gazebo_msgs::GetModelProperties>("/gazebo/get_model_properties");
	 gazebo_msgs::GetModelProperties getmodelproperties; 
	 getmodelproperties.request.model_name= model;
	 if(client.call(getmodelproperties)) 
	    isstatic = getmodelproperties.response.is_static;		
	return isstatic;
}

//Get the current (model) state on the simulator
State get_current_state(bool dim2) const {

    ros::ServiceClient client = n.serviceClient<gazebo_msgs::GetModelState>("/gazebo/get_model_state"); 
    gazebo_msgs::GetModelState getmodelstate;
    strSet::iterator it;
    std::vector<float> tmp_values;
    for(it = _modelsSet.begin(); it != _modelsSet.end(); ++it) {     
      getmodelstate.request.model_name= *it;
      if(client.call(getmodelstate)) 
	 dim_model_state(getmodelstate, tmp_values, dim2);
    }  
    return State(tmp_values);
}

//Model State values depending on 2D or 3D
void dim_model_state(gazebo_msgs::GetModelState getmodelstate, std::vector<float>& state_values, bool dim2) const {
  
   tf::Quaternion q(getmodelstate.response.pose.orientation.x, getmodelstate.response.pose.orientation.y, 
		    getmodelstate.response.pose.orientation.z, getmodelstate.response.pose.orientation.w);
	  tf::Matrix3x3 m(q);
	  double roll, pitch, yaw;
	  m.getRPY(roll, pitch, yaw);  
  state_values.push_back(getmodelstate.response.pose.position.x);
  state_values.push_back(getmodelstate.response.pose.position.y);
  if(!dim2) {
    state_values.push_back(getmodelstate.response.pose.position.z);
    state_values.push_back(yaw);
    state_values.push_back(pitch);
    state_values.push_back(roll);
  }
  else 
    state_values.push_back(roll);
}

//Set the simulator to a given state
void set_sim_to_state(const State& state) const {
    strSet::iterator it;
    for(it = _modelsSet.begin(); it != _modelsSet.end(); ++it) {
		auto idx = std::distance(_modelsSet.begin(), it)*3;
		geometry_msgs::Pose to_pose;
		to_pose.position.x = state[idx];
		to_pose.position.y = state[idx+1];
		to_pose.position.z = 0.5;//TODO: By default, or maybe include it in the state?
		tf::Quaternion q = tf::createQuaternionFromRPY(0.0,0.0,state[idx+2]);
		set_quaternion(to_pose, q);
		
		gazebo_msgs::ModelState modelstate;
		modelstate.model_name = *it;
		modelstate.reference_frame = (std::string) "world";
		modelstate.pose = to_pose;
		
		ros::ServiceClient client = n.serviceClient<gazebo_msgs::SetModelState>("/gazebo/set_model_state");
		gazebo_msgs::SetModelState setmodelstate;
		setmodelstate.request.model_state = modelstate;
		client.call(setmodelstate);	
    }  
}

void set_quaternion(geometry_msgs::Pose& pose, tf::Quaternion& q) const {
	pose.orientation.x = q[0];
	pose.orientation.y = q[1];
	pose.orientation.z = q[2];
	pose.orientation.w = q[3];
}

strSet& models() {return _modelsSet;}


protected:

mutable ros::NodeHandle n;
 strSet _modelsSet;


};

} // namespaces

