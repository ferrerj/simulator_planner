#pragma once


#include <state.hxx>
#include <ros/ros.h>
#include <simulator/gazebo/simulator.hxx>
#include <simulator/gazebo/robot.hxx>
#include <simulator/gazebo/action_komodo.hxx>
#include <simulator/simulator.hxx>



namespace fs0 {

class State; class GroundAction;


class KomodoSimulator : public Simulator {
  
  
private:
  
  mutable ros::NodeHandle _n;
  Robot *_r;
  GazeboSimulator* _gz;
  ActionKomodo *_ak;
  float _factor;
  

  
  
  
public:
  
  KomodoSimulator(const float factor, const Config& config): _factor(factor) {
    
      _r = new Robot(config);
      _gz = new GazeboSimulator(_n);
      _ak = new ActionKomodo(_n);

    
  }
  
  ~KomodoSimulator() {}
  
  State execute(const State& s, const GroundAction& a) const {
    
    set_sim_to_state(s);
    usleep(1000);
    
    
    std::string action = a.getName();
    if(action.compare("move_base") == 0)
      _ak->base_rotation_controller(s[0]+a.getBinding().at(0)*_factor, 1);
    else if(action.compare("move_elbow1")==0)
      _ak->elbow1_controller(s[0]+a.getBinding().at(0)*_factor, 1);
    else if(action.compare("move_elbow2")==0)
      _ak->elbow2_controller(s[0]+a.getBinding().at(0)*_factor, 1);
    else if(action.compare("move_shoulder")==0)
      _ak->shoulder_controller(s[0]+a.getBinding().at(0)*_factor, 1);
    
    State state(get_current_state());
    usleep(1000);
    
    std::cout << state << std::endl;
    
    return state;
    

  }
  
  //only joint
  State get_current_state() const {
     std::vector<float> tmp;
     for(unsigned i = 0; i<_r->joints().size(); ++i) 
      tmp.push_back(_gz->get_joint_properties(_r->joints().at(i)));
     return State(tmp);

  }
  
  void set_sim_to_state(const State& s) const {
   
    _ak->base_rotation_controller(s[0],1);
    _ak->elbow1_controller(s[1],1);
    _ak->elbow2_controller(s[2],1);
    _ak->shoulder_controller(s[3],1);
    usleep(100000);
  }
  
  
  
  
  
  
};



}