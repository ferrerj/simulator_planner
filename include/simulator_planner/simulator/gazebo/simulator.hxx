
#pragma once

#include <state.hxx>
#include <ros/ros.h>
#include <gazebo_msgs/GetModelState.h>
#include <gazebo_msgs/SetModelState.h>
#include <gazebo_msgs/GetWorldProperties.h>
#include <gazebo_msgs/GetModelProperties.h>
#include <gazebo_msgs/GetJointProperties.h>
#include <gazebo_msgs/SetModelConfiguration.h>
#include <gazebo_msgs/GetLinkState.h>
#include <gazebo_msgs/ApplyJointEffort.h>
#include <tf/transform_datatypes.h>
#include "simulator/gazebo/action_simulator.hxx"
#include "simulator/gazebo/sensor_simulator.hxx"
#include "simulator/gazebo/action_komodo.hxx"
#include "dynamic_joint/attached_srv.h"
#include "utils/utils.hxx"


using namespace std;


namespace fs0 { 

class State; class GroundAction;

class GazeboSimulator{
  
public:
  

  
  GazeboSimulator(ros::NodeHandle n):
    _n(n) {}
  
//Get all the model names  
void get_models_from_sim() {
    
  ros::ServiceClient client = _n.serviceClient<gazebo_msgs::GetWorldProperties>("/gazebo/get_world_properties");
  gazebo_msgs::GetWorldProperties getworldproperties; 
   if(client.call(getworldproperties))
 	for(std::vector<std::string>::iterator mit = getworldproperties.response.model_names.begin(); 
	    mit != getworldproperties.response.model_names.end();mit++)
	    if(!isStatic(*mit)) {
		_modelsSet.insert(*mit);
		get_model_properties(*mit);//Get links and joints
	    }
	    
  }
  
  


//Check is static or not
bool isStatic(std::string model) {
	 bool isstatic = false;
	 ros::ServiceClient client = _n.serviceClient<gazebo_msgs::GetModelProperties>("/gazebo/get_model_properties");
	 gazebo_msgs::GetModelProperties getmodelproperties; 
	 getmodelproperties.request.model_name= model;
	 if(client.call(getmodelproperties)) 
	    isstatic = getmodelproperties.response.is_static;
	return isstatic;
}


bool get_model_properties(std::string model) {
   ros::ServiceClient client = _n.serviceClient<gazebo_msgs::GetModelProperties>("/gazebo/get_model_properties");
	 gazebo_msgs::GetModelProperties getmodelproperties; 
	 getmodelproperties.request.model_name= model;
	 if(client.call(getmodelproperties)) {
	  _linksVec = getmodelproperties.response.body_names;
	  _jointsVec = getmodelproperties.response.joint_names;
	  get_graspable_links(model); // Get the graspable links of a model
	 }
	  return getmodelproperties.response.success;
  
}

//Get the graspable links of model
void get_graspable_links(std::string model) {
 
    strVec gLinks;
    std::string link, token;
    std::string delimiter = "_";
  for(unsigned i = 0; i < _linksVec.size(); ++i) {
    link = _linksVec.at(i);
    token = link.substr(0, link.find(delimiter));
    if(token=="gl") 
      gLinks.push_back(link);
  }
     _graspableLinks.insert(std::make_pair(model, gLinks));
}

//Get the properties of a joint
float  get_joint_properties(std::string& name) {
    ros::ServiceClient client = _n.serviceClient<gazebo_msgs::GetJointProperties>("/gazebo/get_joint_properties");
    gazebo_msgs::GetJointProperties getjointproperties;
    getjointproperties.request.joint_name=name;
    if(client.call(getjointproperties)) 
      return (float)getjointproperties.response.position.at(0);
}

//Apply an effort to a join
bool apply_joint_effort(std::string& name, int value){
  ros::ServiceClient client = _n.serviceClient<gazebo_msgs::ApplyJointEffort>("/gazebo/apply_joint_effort");
  gazebo_msgs::ApplyJointEffort applyjointeffort;
  applyjointeffort.request.joint_name = name;
  applyjointeffort.request.effort = value;
  applyjointeffort.request.duration = ros::Duration(2.);
  if(client.call(applyjointeffort))
    return applyjointeffort.response.success;
  
}

//Set a model to a state configuration
//TODO: add string parameter for the name
bool set_model_configuration(const State& state) {
  ros::ServiceClient client = _n.serviceClient<gazebo_msgs::SetModelConfiguration>("/gazebo/set_model_configuration");
  gazebo_msgs::SetModelConfiguration setmodelconf;
  setmodelconf.request.model_name = "tiago_steel";
  for(unsigned i = 0; i < _jointsVec.size(); ++i) {
    setmodelconf.request.joint_names.push_back(_jointsVec.at(i));
   setmodelconf.request.joint_positions.push_back(state[i]); 
  }
    if(client.call(setmodelconf))
      return setmodelconf.response.success;
  
  
}
 

//Get the current (model) state on the simulator
State get_current_state(bool dim2, int discr, bool first) const {

    ros::ServiceClient client = _n.serviceClient<gazebo_msgs::GetModelState>("/gazebo/get_model_state"); 
    gazebo_msgs::GetModelState getmodelstate;
    strSet::iterator it;
    std::vector<float> tmp_values;
    for(it = _modelsSet.begin(); it != _modelsSet.end(); ++it) {     
      getmodelstate.request.model_name= *it;
      if(client.call(getmodelstate)) 
	 dim_model_state(getmodelstate, tmp_values, dim2, discr);
    } 
    
     std::string attached = get_attached_link();
      if(attached.compare("no") == 0) {
	    tmp_values.push_back(0);
      }else {
	
	strSet::iterator it =_modelsSet.find(attached);
	int idx = std::distance(_modelsSet.begin(), it);
	
	tmp_values.push_back(idx);
      }
      
    usleep(1000);
    return State(tmp_values);
}

std::string get_attached_link() const {
  
  ros::ServiceClient client = _n.serviceClient<dynamic_joint::attached_srv>("/attached"); 
  dynamic_joint::attached_srv attached;
  std::string attached_model = "no";
  if(client.call(attached))
    attached_model = attached.response.attached_model;
  
  return attached_model;
}

int get_idx_holding(std::string attached) const {
  
   strSet::iterator it = _modelsSet.find(attached);
  auto idx = std::distance(_modelsSet.begin(), it);
  return _modelsSet.size()*3 + idx -1;
}


//Get the values of all joints of a model
State get_current_configuration(int discr) {
     std::vector<float> tmp_values;
     for(unsigned i = 0; i < _jointsVec.size(); ++i)
      tmp_values.push_back(get_joint_properties(_jointsVec.at(i)));   
     
     return State(tmp_values);
}

//Model State values depending on 2D or 3D
/*
void dim_model_state(gazebo_msgs::GetModelState getmodelstate, std::vector<float>& state_values, bool dim2, int discr) const {
  
   tf::Quaternion q(getmodelstate.response.pose.orientation.x, getmodelstate.response.pose.orientation.y, 
		    getmodelstate.response.pose.orientation.z, getmodelstate.response.pose.orientation.w);
	  tf::Matrix3x3 m(q);
	  double roll, pitch, yaw;
	  m.getRPY(roll, pitch, yaw);  
  state_values.push_back((round(getmodelstate.response.pose.position.x*discr)/discr));
  state_values.push_back((round(getmodelstate.response.pose.position.y*discr)/discr));
  if(!dim2) {
    state_values.push_back((round(getmodelstate.response.pose.position.z*discr)/discr));
    state_values.push_back((round(yaw*discr)/discr));
    state_values.push_back((round(pitch*discr)/discr));
    state_values.push_back((round(roll*discr)/discr));
  }
  else state_values.push_back((round(yaw*discr)/discr));
}
*/

void dim_model_state(gazebo_msgs::GetModelState getmodelstate, std::vector<float>& state_values, bool dim2, int discr) const {
  
   tf::Quaternion q(getmodelstate.response.pose.orientation.x, getmodelstate.response.pose.orientation.y, 
		    getmodelstate.response.pose.orientation.z, getmodelstate.response.pose.orientation.w);
	  tf::Matrix3x3 m(q);
	  double roll, pitch, yaw;
	  m.getRPY(roll, pitch, yaw);  
  state_values.push_back(getmodelstate.response.pose.position.x);
  state_values.push_back(getmodelstate.response.pose.position.y);
  if(!dim2) {
    state_values.push_back(getmodelstate.response.pose.position.z);
    state_values.push_back(yaw);
    state_values.push_back(pitch);
    state_values.push_back(roll);
  }
  else state_values.push_back(yaw);
}


//Set the simulator to a given state
void set_sim_to_state(const State& state) const {
    strSet::iterator it;
    for(it = _modelsSet.begin(); it != _modelsSet.end(); ++it) {
		auto idx = std::distance(_modelsSet.begin(), it)*3;
		geometry_msgs::Pose to_pose;
		to_pose.position.x = state[idx];
		to_pose.position.y = state[idx+1];
		to_pose.position.z = 0.0;//TODO: By default, or maybe include it in the state?
		tf::Quaternion q = tf::createQuaternionFromRPY(0.0,0.0,state[idx+2]);
		set_quaternion(to_pose, q);
				
		gazebo_msgs::ModelState modelstate;
		modelstate.model_name = *it;
		modelstate.reference_frame = (std::string) "world";
		modelstate.pose = to_pose;
		
		ros::ServiceClient client = _n.serviceClient<gazebo_msgs::SetModelState>("/gazebo/set_model_state");
		gazebo_msgs::SetModelState setmodelstate;
		setmodelstate.request.model_state = modelstate;
		client.call(setmodelstate);	
		usleep(10000);
    }
}


void set_model_state(double x, double y, double theta, int idx) const {
		geometry_msgs::Pose to_pose;
		to_pose.position.x = x;
		to_pose.position.y = y;
		to_pose.position.z = 0.0;//TODO: By default, or maybe include it in the state?
		tf::Quaternion q = tf::createQuaternionFromRPY(0.0,0.0,theta);
		set_quaternion(to_pose, q);
		
		strSet::iterator it = _modelsSet.begin();
		std::advance(it, idx);
						
		gazebo_msgs::ModelState modelstate;
		modelstate.model_name = *it;
		modelstate.reference_frame = (std::string) "world";
		modelstate.pose = to_pose;
		
		ros::ServiceClient client = _n.serviceClient<gazebo_msgs::SetModelState>("/gazebo/set_model_state");
		gazebo_msgs::SetModelState setmodelstate;
		setmodelstate.request.model_state = modelstate;
		client.call(setmodelstate);	
		//ros::WallDuration(0.1).sleep();
		usleep(100000);
}

void set_model_state(double x, double y, double theta, std::string model) const {
		geometry_msgs::Pose to_pose;
		to_pose.position.x = x;
		to_pose.position.y = y;
		to_pose.position.z = 0.0;//TODO: By default, or maybe include it in the state?
		tf::Quaternion q = tf::createQuaternionFromRPY(0.0,0.0,theta);
		set_quaternion(to_pose, q);
		
						
		gazebo_msgs::ModelState modelstate;
		modelstate.model_name = model;
		modelstate.reference_frame = (std::string) "world";
		modelstate.pose = to_pose;
		
		ros::ServiceClient client = _n.serviceClient<gazebo_msgs::SetModelState>("/gazebo/set_model_state");
		gazebo_msgs::SetModelState setmodelstate;
		setmodelstate.request.model_state = modelstate;
		client.call(setmodelstate);	
		//ros::WallDuration(0.1).sleep();
		usleep(100000);
}



//Get the link state (coordinates)
 Point2D get_link_state(std::string link) {
  float x, y = 0.0;
   ros::ServiceClient client = _n.serviceClient<gazebo_msgs::GetLinkState>("/gazebo/get_link_state");
  gazebo_msgs::GetLinkState getlinkstate;
  getlinkstate.request.link_name = link;
  getlinkstate.request.reference_frame = (std::string) "world";
  if(client.call(getlinkstate)) {
   x = getlinkstate.response.link_state.pose.position.x;
   y = getlinkstate.response.link_state.pose.position.y;
  }
  Point2D l(x,y);
  return l;
  
}


void set_quaternion(geometry_msgs::Pose& pose, tf::Quaternion& q) const {
	pose.orientation.x = q[0];
	pose.orientation.y = q[1];
	pose.orientation.z = q[2];
	pose.orientation.w = q[3];
}


strSet& models() {return _modelsSet;}
strVec& links() {return _linksVec;}
strVec& joints() {return _jointsVec;}
mapStrVec& graspableLinks() {return _graspableLinks;}
strVec& graspingLinks() {return _graspingLinks;}
int dimension() const {return _dimensions;}


void set_joints(strVec& joints) {_jointsVec=joints;}
void set_links(strVec& links) {_linksVec=links;}

void set_graspableLinks(mapStrVec& graspableLinks) {_graspableLinks = graspableLinks;}
void set_graspingLinks(strVec& graspingLinks) {_graspingLinks = graspingLinks;}
void set_dimension(int dim) {_dimensions = dim;}

protected:
  
 mutable ros::NodeHandle _n;
 strSet _modelsSet;
 strVec _linksVec;
 strVec _jointsVec;
 mapStrVec _graspableLinks;
 strVec _graspingLinks;
 int _dimensions = 2;

};


}