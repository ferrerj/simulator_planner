#include <iostream>
#include <fstream>
#include <vector>
#include "simulator.hxx"
#include <geometry_msgs/Twist.h>



using namespace std;

namespace fs0 {

class Simulator;

class State;
class GroundAction;


//Simple an not general tool to check plans

class PlanPerformer {
  
  
private:
  
  std::string _filename;
  std::vector<fs0::State> _states;
  std::vector<std::pair<string, string>> _actions;
  ros::Publisher _cmd_vel_pub;
  ros::NodeHandle _n;
  GazeboSimulator* _gz;
  ros::ServiceClient _attach_srv;
  ros::ServiceClient _dettach_srv;
  vector<string> _joints;

  

  
  
  
public:
  
 PlanPerformer(std::string filename) : _filename(filename) {
   
  _cmd_vel_pub = _n.advertise<geometry_msgs::Twist>("/cmd_vel", 10); 
   _gz = new GazeboSimulator(_n);
   _attach_srv = _n.serviceClient<gazebo_ros_link_attacher::Attach>("/link_attacher_node/attach");
   _dettach_srv = _n.serviceClient<gazebo_ros_link_attacher::Attach>("/link_attacher_node/detach");
   _joints.push_back("a_robot_big::gripper_big::jleft_finger");
   _joints.push_back("a_robot_big::gripper_big::jgl");
   _joints.push_back("a_robot_big::gripper_big::jright_finger");
   _joints.push_back("a_robot_big::gripper_big::jgr");


}
 virtual ~PlanPerformer() {}
 
 
 //Only for 2D, no joints
 void read_states() {
      
  std::string line, state;
  std::vector<float> tmp;
  float v;
  std::ifstream myfile (_filename);
  if (myfile.is_open()) {
    while ( getline (myfile,line) ) {
      std::stringstream ss(line);
  
      while (ss >> v) {
	  tmp.push_back(v);
	  if (ss.peek() == ' ')
	      ss.ignore();
      }
      State s(tmp);
      _states.push_back(s);
      tmp.clear();
    }
    myfile.close();
  }  

    
   
 }
 
 void set_robot_state() {
   GazeboSimulator gz(_n);
   for(unsigned i = 0; i<_states.size();++i) {
    gz.set_model_state(_states.at(i)[0], _states.at(i)[1], _states.at(i)[2], "a_robot_big");
   usleep(750000);
   }
   
 }
 
 
 void read_action() {
    std::string line;
    std::ifstream myfile (_filename);
  if (myfile.is_open()) {
    while ( getline (myfile,line) ) {
     std::string delimiter = " ";
     size_t pos = 0;
    std::string token1, token2;
    while ((pos = line.find(delimiter)) != std::string::npos) {
      token1 = line.substr(0, pos);
      token2 = line.substr(pos+1);
      _actions.push_back(std::make_pair(token1,token2));
      line.erase(0, pos + delimiter.length());
}
    }

  }
 }
 
 void perform_plans() {
   
   for(unsigned i = 0; i < _actions.size(); ++i) {
    
    string action = _actions.at(i).first;
    string param = _actions.at(i).second;
    
    if(action == "move") {
      if(param == "f")
	move(0);
      else if(param == "cw")
	move(2);
      else if(param == "ccw")
	move(1);
      
    }
    else if(action == "grasp") {
      grasp(param);
      
    }
    else if(action == "place") {
      place(param);
      
    }

   }

 }
 
 void move(int dir) {
   cout << "MOVE " << dir << endl;
   ActionSimulator::cmd_vel_simple_action(dir, 1, 0.523599, 1.0, _cmd_vel_pub ); 
 }
 
 
 void grasp(string an_obj) {
   cout << "GRASP " << an_obj << endl;
   std::vector<float> binding;
  binding.push_back(1.0);
  binding.push_back(0.0);
  binding.push_back(1.0);
  GroundAction a(0, binding);
    double angle = 100;
    double dist = 100; 
    Point<double, 2> bug = _gz->get_link_state("a_robot_big::bug");
    Point<double, 2> palm = _gz->get_link_state("a_robot_big::gripper_big::palm");
    string lObj = an_obj+"::gl_";
    cout << lObj << endl;
    Point<double, 2> obj = _gz->get_link_state(lObj);
    
    cout << "Bug: " << bug[0] << ", " << bug[1] << endl;
    cout << "Palm: " << palm[0] << ", " << palm[1] << endl;
    cout << "Bug: " << obj[0] << ", " << obj[1] << endl;

    angle = Geometry::angle_between(bug, palm, obj);
    cout << "ANGLE: " << angle << endl;
    ActionSimulator::cmd_vel_action(a, 0.0, angle, 1.0, _cmd_vel_pub);
    
    
    
    usleep(1000);
    while( (dist >= 0.11) ) {    
     Point<double, 2> palm = _gz->get_link_state("a_robot_big::gripper_big::palm");
     dist = Geometry::distance(palm, obj) -0.2;
     cout << "DIST: " << dist << endl;
     ActionSimulator::cmd_vel_action(a, dist, 0.0, 1.0, _cmd_vel_pub);
   }
   ActionSimulator::close_gripper(_joints , _n);
   ActionSimulator::grasp_attaching(an_obj, _attach_srv);
   usleep(1000);

 }
 
 void place(string obj) {
   cout << "PLACE " << obj << endl;
   ActionSimulator::open_gripper(_joints, _n);
   ActionSimulator::place_dettaching(obj, _dettach_srv); 
   
  std::vector<float> binding;
  binding.push_back(-1.0);
  binding.push_back(0.0);
  binding.push_back(0.0);
  GroundAction a(0, binding);
  ActionSimulator::cmd_vel_action(a, 0.78 , 0.0 , 1, _cmd_vel_pub); 
  usleep(10000);

   
 }
 
 
 
 
  
  
  
  
  
  
};

}