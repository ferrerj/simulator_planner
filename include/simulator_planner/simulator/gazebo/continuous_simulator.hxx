
#pragma once


#include <state.hxx>
#include <ros/ros.h>
#include <gazebo_msgs/GetModelState.h>
#include <gazebo_msgs/SetModelState.h>
#include <gazebo_msgs/GetWorldProperties.h>
#include <gazebo_msgs/GetModelProperties.h>
#include <tf/transform_datatypes.h>
#include <simulator/simulator.hxx>
#include <simulator/gazebo/simulator.hxx>
#include <simulator/gazebo/robot.hxx>
#include <geometry_msgs/Twist.h>
#include <unistd.h>
#include <math.h>


namespace fs0 {
  
class State; class GroundAction;

class ContinuousGazeboSimulator : public Simulator {

public:
  
  typedef std::vector<Point2D> pVec;
  

  ContinuousGazeboSimulator(const float v_trans, const float v_rot, const float deltaT, const int discr, const Config& config):
      _v_trans(v_trans), _v_rot(v_rot), _deltaT(deltaT), _discr(discr) {
      _cmd_vel_pub = n.advertise<geometry_msgs::Twist>("/cmd_vel", 10);
      _joint_traj_pub = n.advertise<trajectory_msgs::JointTrajectory>("/arm_controller/command", 100);
      _collision_client = n.serviceClient<dynamic_joint::collision>("/collide");
      _attach_srv = n.serviceClient<dynamic_joint::attach>("/attach");
      _r = new Robot(config);
      gz = new GazeboSimulator(n);
  }
      
  ContinuousGazeboSimulator() {
  	_cmd_vel_pub = n.advertise<geometry_msgs::Twist>("/cmd_vel", 10);
  }
    
  ~ContinuousGazeboSimulator() {}
 
  State execute(const State& s, const GroundAction& a) const {
  std::string action = a.getName();
  
    //std::cout << "Action to apply: " << action << std::endl;
  
      if(action == "")
	return s;
     else if(action == "move")
     return execute_cmd(s,a);
    else if((action == "grasp") && (s[idx_obj_holding] == 0))
      return execute_grasp(s,a);
     else if((action == "place") && (s[idx_obj_holding] != 0))
       return execute_place(s,a);
     else if(action == "move_arm") 
       return execute_arm(s,a);
      else return s;
  }
  /*
  State execute_cmd(const State& s, const GroundAction& a) const {
    
    gz->set_sim_to_state(s);
    check_grasp(s);    
    
    if(s[idx_obj_holding] != 0)
      gz->set_model_state(100, 100, 0.0, s[idx_obj_holding]);
    
   
    ActionSimulator::cmd_vel_action(a, _v_trans, _v_rot, _deltaT, _cmd_vel_pub);
    
    State state(gz->get_current_state(true, _discr, false));
    if(SensorSimulator::collide(_collision_client)) {
      std::cout << "Robot has collided " << std::endl;
     return s; 
    }
      if(SensorSimulator::free_path(state, s, idx_obj_holding, _holding) == false)//Just if there is only 1 object!!!!!! Bad bad....
	return s;
      else return state;
  }
  */
  
  State execute_cmd(const State& s, const GroundAction& a) const {

  
      if(s[idx_obj_holding] != 0)
	return execute_cmd_obj(s, a);
      else 
	return execute_cmd_no_obj(s, a);
  
  }
  
  State execute_cmd_obj(const State& s, const GroundAction& a) const {
     gz->set_sim_to_state(s);
     usleep(10000);
    _holding=true;
     gz->set_model_state(100, 100, 0.0, s[idx_obj_holding]);
     ActionSimulator::cmd_vel_action(a, _v_trans, _v_rot, _deltaT, _cmd_vel_pub);
     usleep(10000);
     if(SensorSimulator::collide(_collision_client)) 
      return s; 
     
     pVec fingers;
     GraspingLinks_state(fingers);
     Point2D mid = Geometry::between_points(fingers.at(0), fingers.at(1));
     gz->set_model_state(mid[0], mid[1], 0.0, s[idx_obj_holding]);
     
     usleep(10000);
     
     if(SensorSimulator::collide(_collision_client)) 
      return s; 
     
     State state(gz->get_current_state(true, _discr, false));
     usleep(10000);
     state.set_value(idx_obj_holding,s[idx_obj_holding]);
  
     if(SensorSimulator::free_path(state, s, idx_obj_holding, _holding) == false)
	return s;
     else return state;

     return state;
      
  }
  
  State execute_cmd_no_obj(const State& s, const GroundAction& a) const {
      gz->set_sim_to_state(s);
      usleep(10000);
      ActionSimulator::cmd_vel_action(a, _v_trans, _v_rot, _deltaT, _cmd_vel_pub);
      usleep(10000);
      if(SensorSimulator::collide(_collision_client)) 
       return s; 
       
      State state(gz->get_current_state(true, _discr, false));
      usleep(1000);
      _holding=false;
        
   
     if(SensorSimulator::free_path(state, s, idx_obj_holding, _holding) == false) 
	return s;
     else return state;
      
     return state;
  }
  
  
  State execute_arm(const State& s, const GroundAction& a) const {
    State state(s);
    gz->set_model_configuration(s);
    ActionSimulator::arm_command(a, gz->joints(), _joint_traj_pub);
    return gz->get_current_configuration(_discr);
    
  }
  
  void check_grasp(const State& s) const {
    
   if( (int) s[idx_obj_holding] != 0) {
    strSet::iterator it = models().begin();
    std::advance(it, s[idx_obj_holding]);
    if(_holding==false) {
      std::string link = *it+"::gl_";
      ActionSimulator::grasp(m1, l1, *it, link, _attach_srv);
     _holding=true; 
    }
   }
   else {
    if(_holding==true) {
      ActionSimulator::place(_attach_srv);
      usleep(10000);
      _holding=false;
      
    }
   }
   
  }
  
/*
  State execute_grasp(const State& s, const GroundAction& a) const  {
  gz->set_sim_to_state(s); 
 int idx = a.getBinding().at(1);//Get the index of the object/model to be grasped
   strSet::iterator it = models().begin();
   std::advance(it, idx);//"it" contains the name of the object to be grasped
    constexpr int d = 2;
    Point< double, d > o;
    strVec graspable = graspableLinks()[*it];
    pVec fingers;
    if(SensorSimulator::at_distance(s, idx, 1.2, 0.8))
      get_ready_to_grasp(s, idx);
    else return s;
    GraspingLinks_state(fingers);
    for(unsigned i = 0; i < graspable.size(); ++i) {
     GraspableLink_state(o, *it+"::"+graspable.at(i));
     std::cout << "Graspable: " << graspable.at(i) << std::endl;
     if(SensorSimulator::is_between_fingers(fingers, o)) {
       m1 = "a_robot_big";
       l1 = "a_robot_big::gripper_big::palm";
       m2 = *it;
       l2 = graspable.at(i);
      ActionSimulator::grasp("a_robot_big", "a_robot_big::gripper_big::palm", *it, graspable.at(i), _attach_srv );
      State state(gz->get_current_state(true, _discr, false));
      state.set_value(idx_obj_holding, idx);
      _holding=true;

         return state; 

    }
    return s;
  }
  }
  */

  State execute_grasp(const State& s, const GroundAction& a) const  {
    gz->set_sim_to_state(s);
    usleep(1000);
    int idx = a.getBinding().at(0);//Get the index of the object/model to be grasped
    strSet::iterator it = models().begin();
    std::advance(it, idx);//"it" contains the name of the object to be grasped
    constexpr int d = 2;
    Point< double, d > o;
    strVec graspable = graspableLinks()[*it];
    pVec fingers;
    if(SensorSimulator::at_distance(s, idx, _r->grasp_dist().first, _r->grasp_dist().second)) {
      if(!get_ready_to_grasp(s, idx))
	return s;
      }
    else return s;
    GraspingLinks_state(fingers);
    for(unsigned i = 0; i < graspable.size(); ++i) {
     GraspableLink_state(o, *it+"::"+graspable.at(i));
     if(SensorSimulator::is_between_fingers(fingers, o)) {
	State state(gz->get_current_state(true, _discr, false));
	usleep(1000);
	state.set_value(idx_obj_holding, idx);
	_holding=true;
      return state; 

    }
    return s;
  }
  }
/*  
  State execute_place(const State& s, const GroundAction& a) const {
    //State state(s);
     gz->set_sim_to_state(s);
    ActionSimulator::place(_attach_srv);
    usleep(10000);
    get_ready_to_place();
    State state(gz->get_current_state(true, _discr, false));
    state.set_value(idx_obj_holding,0);
    _holding=false;
    return state;
  }
  
  */

  State execute_place(const State& s, const GroundAction& a) const {
    gz->set_sim_to_state(s);
    usleep(1000);
    get_ready_to_place();
    if(SensorSimulator::collide(_collision_client)) 
       return s; 
    State state(gz->get_current_state(true, _discr, false));
    usleep(1000);
    state.set_value(idx_obj_holding,0);
    _holding=false;
    return state;
  }


  void get_ready_to_place() const {
   
  std::vector<float> binding;

  binding.push_back(-1.0);
  binding.push_back(0.0);
  binding.push_back(0.0);
  GroundAction a(0, binding);
  ActionSimulator::cmd_vel_action(a, (_v_trans) , 0.0 , _deltaT, _cmd_vel_pub); 
  usleep(10000);

  }
  
  
bool get_ready_to_grasp(const State& s, int obj_idx) const {
  
  int idx = 3*obj_idx;
  double angle = 100;
  double dist = 100; 
  std::vector<float> binding;
  
  binding.push_back(1.0);
  binding.push_back(0.0);
  binding.push_back(1.0);
  GroundAction a(0, binding);
  int factor = 4;
  int trials=0;

    Point<double, 2> robot(s[0], s[1]);
    std::string base = _r->robot()+"::"+_r->base();
    Point<double, 2> bug= gz->get_link_state(base);
    pVec fingers;
    GraspingLinks_state(fingers);
    Point2D mid = Geometry::between_points(fingers.at(0), fingers.at(1));
    std::string hand = _r->robot()+"::"+_r->gripper()+"::"+_r->hand();
    Point<double, 2> palm = gz->get_link_state(hand);
    Point<double, 2> obj(s[idx], s[idx+1]);
    
    angle = Geometry::angle_between(bug, palm, obj);

    ActionSimulator::cmd_vel_action(a, 0.0, angle*factor, 1.0/factor, _cmd_vel_pub); 
   if(SensorSimulator::collide(_collision_client)) 
     return false; 
    
    
    usleep(1000);

  while( (dist >= 0.11) ) {    
     Point<double, 2> palm = gz->get_link_state(hand);
     dist = Geometry::distance(palm, obj) -0.2;
     ActionSimulator::cmd_vel_action(a, dist*factor, 0.0, 1.0/factor, _cmd_vel_pub);
    if(SensorSimulator::collide(_collision_client)) 
     return false; 
    
     
     trials++;
     if(trials > 3) break;
    }
    

     State state(gz->get_current_state(true, _discr, false));

     if(SensorSimulator::free_path(state, s, idx_obj_holding, _holding) == false)
      return false;
     else return true;
    
    return true;

    
 }
 
  void GraspingLinks_state(pVec& vPoint) const {
   for(unsigned i = 0; i < graspingLinks().size(); ++i) 
     vPoint.push_back(gz->get_link_state(graspingLinks().at(i)));  
  }   
  
  void GraspableLink_state(Point2D& o, std::string link) const {
     o = gz->get_link_state(link);
  }
  
  State get_current_state(bool dim2, bool first) const {
    return gz->get_current_state(dim2, _discr, first);
  } 
  void get_models_from_sim() {
    gz->get_models_from_sim();
    idx_obj_holding = models().size()*3;
}
 void set_joints(strVec& joints) {
   gz->set_joints(joints);
}

void set_graspingLinks(strVec& graspingLinks) {gz->set_graspingLinks(graspingLinks);}

mapStrVec graspableLinks() const {return gz->graspableLinks();}
strVec graspingLinks() const {return gz->graspingLinks();}
strSet& models() const {return gz->models();}
int dimension() {return gz->dimension();}

void set_problem_type(unsigned problem_type) {_problem_type = problem_type;}
 

protected:
  
mutable ros::NodeHandle n;
float _v_trans;
float _v_rot;
float _deltaT;
unsigned int _discr;
ros::Publisher _cmd_vel_pub;
ros::Publisher _joint_traj_pub;
mutable ros::ServiceClient _attach_srv;
ros::ServiceClient _collision_client;
mutable bool _holding = false;
mutable int idx_obj_holding;
mutable pVec _fingers;
mutable std::string m1, l1, m2, l2;
Robot *_r;


strSet _modelsSet;
GazeboSimulator* gz;
unsigned _problem_type;
ros::Publisher _attach_pub;


};


  
  
  
  
}