#pragma once

#include <state.hxx>
#include <ros/ros.h>
#include "utils/utils.hxx"
#include "dynamic_joint/collision.h"


namespace fs0 {

class State; class GroundAction;


class SensorSimulator {
  
  
public:
  
  SensorSimulator() {}
  ~SensorSimulator() {}
 
  
  static bool is_between_fingers(std::vector<Point2D>& fingers, Point2D obj ) {
        
	double dist_f1 = Geometry::distance(fingers.at(0), obj);
	double dist_f2 = Geometry::distance(fingers.at(1), obj);
	//std::cout << "Dist: " << dist_f1 << "---" << dist_f2 << std::endl;

    if(dist_f1 <= 0.38)
      if(dist_f2 <= 0.38)
	return true;
      
    return false;

  }
  
  static bool in_front_hand(Point2D& hand, Point2D obj) {
   
    double dist = Geometry::distance(hand, obj);
    std::cout << "Distance: " << dist << std::endl;
    
  }
  
  //Check if a model is at an evaluation distance.
  //If it is, is a candidate to be grasped
  static bool at_distance(const State& s, int i, const double min_eval_dist, double max_eval_dist) {
    Point2D r(s[0], s[1]);
    int idx = i*3;
    Point2D o(s[idx], s[idx+1]);
    //std::cout << "evaluation distance: " << Geometry::distance(r,o) << std::endl;
    if( (Geometry::distance(r,o) >= min_eval_dist) && (Geometry::distance(r,o) <= max_eval_dist) ) return true;
    else return false;
        
  }

  
  //Check if the robot collides with an object, which is not the object held
  //idx_obj_holding is the object being held
  static bool free_path(const State& s1, const State& s2, int idx_obj_holding, bool holding) {

    bool equal = true;
    double epsilon = 0.001;
    int idx = 3*s1[idx_obj_holding]; 
      for(unsigned i = 3; i < s1.size(); ++i) {
	  equal = (fabs(s1[i] - s2[i]) < epsilon);
	  if(equal == false && ((i < idx) || (i > idx+2)) ) { 
	    return equal;
	  } 
	}

    
    return true;	
 }
 
 
 //Check when robot collides with an static element
 static bool collide(ros::ServiceClient _collision_client) {
  
   
   bool robot_collision = false;
   dynamic_joint::collision collide;
   if(_collision_client.call(collide))
    robot_collision = collide.response.collision;
   
   //std::cout << "Collision: " << robot_collision << std::endl;
   return robot_collision;
   
 }
 
  
  
};

}