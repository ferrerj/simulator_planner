
#include <atom_range.hxx>
#include <iostream>

namespace fs0 {


std::ostream& AtomRange::print(std::ostream& os) const {
 	//const ProblemInfo& problemInfo = Problem::getInfo();
	os << "{" << _variable << " in [" << _range.first << ", " << _range.second << "]}";
	return os;
}


void AtomRange::print() {
 
    std::cout << "{" << _variable << ": [" << _range.first << ", " << _range.second << "]}" << std::endl;
  
}


} // namespaces
