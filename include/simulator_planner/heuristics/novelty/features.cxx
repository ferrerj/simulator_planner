
#include <heuristics/novelty/features.hxx>
#include <state.hxx>

namespace fs0 {

aptk::ValueIndex StateVariableFeature::evaluate(const State& s) const { 
	//std::cout << s << std::endl;
	//for(unsigned i = 0; i < s.size(); ++i)
	//  std::cout << _discretizer.discretize(i, s[i]) << std::endl;
	return _discretizer.discretize(_variable, s[_variable]);
}

} // namespaces