
#include <iostream>
#include <set>

#include <heuristics/novelty/fs0_novelty_evaluator.hxx>
#include <utils/printers/feature_set.hxx>

namespace fs0 {

GenericStateAdapter::GenericStateAdapter( const State& s, const GenericNoveltyEvaluator& featureMap )
	: _adapted( s ), _featureMap( featureMap ) {}

GenericStateAdapter::~GenericStateAdapter() {}

GenericNoveltyEvaluator::GenericNoveltyEvaluator(const Problem& problem, unsigned novelty_bound)
	: Base()
{
	set_max_novelty(novelty_bound);
	selectFeatures(problem);

}

GenericNoveltyEvaluator::GenericNoveltyEvaluator(): Base() {}


GenericNoveltyEvaluator::~GenericNoveltyEvaluator() {
	for ( NoveltyFeature::ptr f : _features ) delete f;
}

void GenericNoveltyEvaluator::selectFeatures(const Problem& problem) {
	unsigned num_variables = problem.getInitialState().size();
	//std::cout << "Num of variables: " << num_variables << std::endl;
	if(_features.empty())
	  for (VariableIdx x = 0; x < num_variables; ++x) 
		_features.push_back(new StateVariableFeature(x, Discretizer()));
	
	//std::cout << "Features: " << _features.size() << std::endl;
}

void GenericStateAdapter::get_valuation(std::vector<aptk::VariableIndex>& varnames, std::vector<aptk::ValueIndex>& values) const {
	auto n = _featureMap.numFeatures();
	varnames.resize(n);
	values.resize(n);
      //	std::cout << "Features2: " << _featureMap.numFeatures() << std::endl;

	for (unsigned k = 0; k < n; ++k) {
		varnames[k] = k;
		values[k] = _featureMap.feature( k )->evaluate( _adapted );
	}

}

} // namespaces
