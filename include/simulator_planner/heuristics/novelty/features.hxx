
#pragma once

#include <types.hxx>
#include <aptk2/heuristics/novelty/fd_novelty_evaluator.hxx>
#include <simulator/discretization.hxx>

namespace fs0 {
	
class State; class Discretizer;

//! Base interface for any novelty feature
class NoveltyFeature {
public:
	typedef NoveltyFeature* ptr;

	virtual ~NoveltyFeature() {}
	
	virtual NoveltyFeature* clone() = 0;
	virtual aptk::ValueIndex evaluate( const State& s ) const = 0;
};

//! A state variable-based feature that simply returs the value of a certain variable in the state
class StateVariableFeature : public NoveltyFeature {
public:
	StateVariableFeature(VariableIdx variable, const Discretizer& discretizer) : _variable(variable), _discretizer(discretizer) {}
	~StateVariableFeature() {}
	
	StateVariableFeature(const StateVariableFeature& other) = default;
	
	aptk::ValueIndex evaluate(const State& s) const;

	NoveltyFeature* clone() { return new StateVariableFeature(*this); }
protected:
	VariableIdx _variable;
	
	const Discretizer _discretizer;
};


} // namespaces
