
#pragma once

#include <actions/ground_action.hxx>
#include <state_model.hxx>
#include <problem.hxx>

namespace fs0 {

//! The heuristic value of any given state is the number of unsatisfied goal conditions (atoms) on that state
class UnsatisfiedGoalAtomsHeuristic {
public:
	typedef GroundAction Action;

	UnsatisfiedGoalAtomsHeuristic(const SimulatorStateModel& model) : _problem(model.getProblem()) {}
	
	//! The actual evaluation of the heuristic value for any given non-relaxed state s.
	float evaluate(const State& state) const { 
		unsigned unsatisfied = 0;
		for (auto range_condition:_problem.getGoalConditions()) {
			if (!state.satisfies(range_condition)) ++unsatisfied;
		}
		return unsatisfied;
	}
	
/*
	float evaluate_unsat(const State& state, std::vector<AtomRange>& goal_range) const { 
		unsigned unsatisfied = 0;
		for (auto range_condition:goal_range) {
			if (!state.satisfies(range_condition)) ++unsatisfied;
		}
		return unsatisfied;
	}*/
/*
	float evaluate(const State& state) const { 
	      unsigned unsatisfied = 0;
	      for(auto& it: _problem.getGoalConditions())
		unsatisfied = evaluate_unsat(state, it.second);	    
	      
	      return unsatisfied;
	}
	*/
	
protected:
	//! The actual planning problem
	const Problem& _problem;
};

} // namespaces
