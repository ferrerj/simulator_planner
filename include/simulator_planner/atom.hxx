
#pragma once

#include <types.hxx>
#include <iostream>

namespace fs0 {

//! An atom is a tuple X=x, where X is a state variable and x a value from its domain.
class Atom {
protected:
	//! The state variable
	VariableIdx _variable;

	//! The domain value.
	float  _value;
	
public:

	Atom(VariableIdx variable, float value) : _variable(variable), _value(value) {};
	Atom(const Atom& other) = default;
	Atom(Atom&& other) = default;

	Atom& operator=(const Atom& other) = default;
	Atom& operator=(Atom&& other) = default;

	inline VariableIdx getVariable() const { return _variable; }
	inline float getValue() const { return _value; }

	std::ostream& print(std::ostream& os) const;
	friend std::ostream& operator<<(std::ostream &os, const Atom& atom) { return atom.print(os); }
	void print() {std::cout << _variable << ": " << _value << std::endl;}
};

//! Comparison operators
inline bool operator==(const Atom& lhs, const Atom& rhs){ return lhs.getVariable() == rhs.getVariable() && lhs.getValue() == rhs.getValue(); }
inline bool operator!=(const Atom& lhs, const Atom& rhs){return !operator==(lhs,rhs);}
inline bool operator< (const Atom& lhs, const Atom& rhs){
	if (lhs.getVariable() < rhs.getVariable()) return true;
	if (lhs.getVariable() > rhs.getVariable()) return false;
	if (lhs.getValue() < rhs.getValue()) return true;
	return false;
}
inline bool operator> (const Atom& lhs, const Atom& rhs){return  operator< (rhs,lhs);}
inline bool operator<=(const Atom& lhs, const Atom& rhs){return !operator> (lhs,rhs);}
inline bool operator>=(const Atom& lhs, const Atom& rhs){return !operator< (lhs,rhs);}


} // namespaces
