
#include <iostream>
#include <cassert>

#include <state.hxx>
#include <problem.hxx>
#include "atom_range.hxx"

namespace fs0 {
	

std::ostream& State::print(std::ostream& os) const {
	os << "State";
	os << "(" << _hash << ")={";
	for (unsigned i = 0; i < _values.size(); ++i) {
		os << _values.at(i);
		if (i < _values.size() - 1) os << ", ";
	}
	os << "}";
	return os;
}

bool State::satisfies(const AtomRange& range) const {
	auto value = _values[range.getVariable()];
	auto _range = range.getRange();
	return value >= _range.first && value <= _range.second;
}


} // namespaces
