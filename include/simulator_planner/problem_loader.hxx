#include <utils/loader.hxx>
#include <iostream>
#include <fstream>

namespace fs0 {

class ProblemLoader {
  
private:
  

   
public:
  
	ProblemLoader() {

	}

	~ProblemLoader() {}
  
  	std::unique_ptr<Problem> loadProblem(std::string& filename, State&& init) {
	  
	  auto schemata = loadActionSchemata(filename +  "_actions.json");
	  auto actions = loadGroundedActions(filename +  "_actions.json", schemata);
	  auto goals = loadGoalConditions(filename + "_goals.json");	  
	  auto problem = new Problem("domain", "instance", schemata, actions, init, goals);
	  
	  
	  for(unsigned i = 0; i < problem->getGoalConditions().size(); ++i) 
	    std::cout << problem->getGoalConditions().at(i).getVariable() << "->" << problem->getGoalConditions().at(i).getRange().first  << std::endl;
	  
	  return std::unique_ptr<Problem>(problem);
	}
	
	//Load the used joints from the robot (usually the arm joints)
	std::vector<std::string> loadUsedJoints(std::string& filename) {
	  std::vector<std::string> joints;
	  rapidjson::Document data = loadJSONObject(filename);
	  rapidjson::Value& value_joints = data["joints"];
	  for(unsigned i=0; i < value_joints.Size(); i++)  
	    joints.push_back(value_joints[i].GetString());
	  return joints; 
	}
		
	AtomRange loadVariablesRanges(const VariableIdx var, const rapidjson::Value& data) {
	  assert(data.IsArray());
	  std::unique_ptr<AtomRange> ar(new AtomRange(var, std::make_pair(data[0].GetDouble(), data[1].GetDouble())));
	  return *ar;
}
	
	std::vector<AtomRange> loadGoalConditions(const std::string& filename ) {
	  rapidjson::Document data = loadJSONObject(filename);
	  std::vector<AtomRange> ranges;
	  
	  ranges.push_back(loadVariablesRanges(0,data["x"]));
	  ranges.push_back(loadVariablesRanges(1,data["y"]));
	  ranges.push_back(loadVariablesRanges(2,data["yaw"]));
	  
	  /*
	  ranges.push_back(loadVariablesRanges(0,data["base"]));
	  ranges.push_back(loadVariablesRanges(1,data["elbow1"]));
	  ranges.push_back(loadVariablesRanges(2,data["elbow2"]));
	  ranges.push_back(loadVariablesRanges(3,data["shoulder"]));
	  ranges.push_back(loadVariablesRanges(4,data["wrist"]));
	  
	  */
	  return ranges; 	  
	}
	
	
	std::vector<ActionSchema::cptr> loadActionSchemata(const std::string& filename) {
		std::vector<ActionSchema::cptr> schemata;
		rapidjson::Document data = loadJSONObject(filename);
		rapidjson::Value& value_actions = data["actions"];
		for (unsigned a = 0; a < value_actions.Size(); ++a) {
		  std::string name = value_actions[a]["name"].GetString();
		  rapidjson::Value& value_signature = value_actions[a]["signature"];
		  rapidjson::Value& value_parameters = value_actions[a]["parameters"];
		  schemata.push_back(new ActionSchema(name, parseStringList(value_signature), parseStringList(value_parameters)));
		}
	  return schemata;
	}

  std::vector<GroundAction::cptr> loadGroundedActions(const std::string& filename, std::vector<ActionSchema::cptr> schema ) {
	std::vector<GroundAction::cptr> actions;
	rapidjson::Document data = loadJSONObject(filename);
	rapidjson::Value& value_actions = data["actions"];
	for (unsigned a = 0; a < value_actions.Size(); ++a) {
	  unsigned id = value_actions[a]["id"].GetInt();
	  rapidjson::Value& value_values = value_actions[a]["values"];
	  actions.push_back(new GroundAction(id, parseDoubleValues(value_values), schema.at(id)));
	}
	  return actions;
	 }

	 
  std::vector<std::string> parseStringList(const rapidjson::Value& data) {
	std::vector<std::string> output;
	assert(data.IsArray());
	for (unsigned i = 0; i < data.Size(); ++i) 
		output.push_back(data[i].GetString());
	
	return output;
}

  std::vector<float> parseDoubleValues(const rapidjson::Value& data) {  
    std::vector<float> output;
    assert(data.IsArray());
    for (unsigned i = 0; i < data.Size(); ++i) 
      output.push_back(data[i].GetDouble());
    
    return output;
  
}


	
	/*
	Goals loadGoalConditions(const std::string& filename ) {
	  	  Goals goal;

	  rapidjson::Document data = loadJSONObject(filename);
	  std::vector<AtomRange> ranges;
	  ranges.push_back(loadVariablesRanges(0,data["x"]));
	  ranges.push_back(loadVariablesRanges(1,data["y"]));
	  ranges.push_back(loadVariablesRanges(2,data["yaw"]));
	  goal.insert(std::make_pair(0, ranges));
	  return goal; 	  
	}
	
	*/
	//Load the goal conditions (non-atomic)
	/*Goals loadGoalConditions(const std::string& filename) {
	  rapidjson::Document data = loadJSONObject(filename);
	  Goals goal;
	  rapidjson::Value& goals = data["goals"];
	  VariableIdx idx = 0;
	  for(unsigned i=0; i < goals.Size(); i++) {
	    rapidjson::Value& agoal = goals[i];
	     std::vector<AtomRange> ranges;
	     for(unsigned j=0; j < _state_vars.size(); j++) {
	       std::string var = _state_vars.at(j);
	      ranges.push_back(loadVariablesRanges(idx,agoal[var.c_str()]));
	      idx++;
	     }
	    goal.insert(std::make_pair(agoal["model"].GetInt(), ranges));
	  }
	  
	  return goal;
	  
	}*/

	rapidjson::Document loadJSONObject(const std::string& filename) {
	// Load and parse the JSON data file.
	std::ifstream in(filename);
	if (in.fail()) throw std::runtime_error("Could not open filename '" + filename + "'");
	std::string str((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>());
	rapidjson::Document data;
	data.Parse(str.c_str());
	return data;
}

};

}