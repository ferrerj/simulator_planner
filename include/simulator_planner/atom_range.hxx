
#pragma once

#include <cstdint>
#include <vector>
#include <boost/container/flat_set.hpp>
#include <tuple>

#include <types.hxx>

namespace fs0 {

class AtomRange {

protected:
	//! The state variable
	VariableIdx _variable;

	//! The range
	Range<float> _range;
	
public:
	AtomRange(const VariableIdx variable, const Range<float>& range) : _variable(variable), _range(range) {}
	AtomRange(const VariableIdx variable, const Range<float>&& range) : _variable(variable), _range(std::move(range)) {}
	
	AtomRange(const AtomRange& other) = default;
	AtomRange(AtomRange&& other) = default;

	AtomRange& operator=(const AtomRange& other) = default;
	AtomRange& operator=(AtomRange&& other) = default;
	
	VariableIdx getVariable() const { return _variable; }
	const Range<float>& getRange() const { return _range; }

	std::ostream& print(std::ostream& os) const;
	friend std::ostream& operator<<(std::ostream &os, const AtomRange& atom) { return atom.print(os); }
	void print();

};


} // namespaces
