
#include <atom.hxx>
#include <problem.hxx>

namespace fs0 {

std::ostream& Atom::print(std::ostream& os) const {
 //	const ProblemInfo& problemInfo = Problem::getInfo();
	os << "[" << _variable << "=" << _value << "]";
	return os;
}


} // namespaces
