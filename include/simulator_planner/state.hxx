
#pragma once

#include <iostream>
#include <cstdlib>
#include <memory>
#include <set>
#include <vector>
#include <boost/functional/hash.hpp>
#include <types.hxx>

namespace fs0 {

class AtomRange;
class Problem;

class State {
protected:
	//! Each position in the vector is a x,y or theta value corresponding to a model of the gazebo simulator
	std::vector<float> _values;
	
	//! Cache the hash value of the state for performance reasons
	std::size_t _hash;

public:
	typedef std::shared_ptr<const State> cptr;

	// The only allowed constructor receives all values making up the state
	State(std::vector<float> values) : _values(values), _hash(computeHash()) {}
	~State() {}
	
	//! Default copy constructors and assignment operators
	State(const State& state) = default;
	State( State&& state ) = default;
	State& operator=(const State &state) = default;
	State& operator=(State&& state) = default;

	// Check the hash first for performance.
	bool operator==(const State &rhs) const { return _hash == rhs._hash && _values == rhs._values; }
	bool operator!=(const State &rhs) const { return !(this->operator==(rhs));}
	
	float& operator[](unsigned idx) { return _values[idx]; }
	const float operator[](unsigned idx) const { return _values[idx]; }

	inline std::size_t size() const { return _values.size(); }

	//! Prints a representation of the state to the given stream.
	friend std::ostream& operator<<(std::ostream &os, const State&  state) { return state.print(os); }
	std::ostream& print(std::ostream& os) const;
	
	std::size_t hash() const { return _hash; }
	
	bool satisfies(const AtomRange& range) const;
	void set_value(unsigned idx, float value) {_values[idx] = value;}
	
protected:
	void updateHash() { _hash = computeHash(); }
	
	std::size_t computeHash() const { return boost::hash_range(_values.begin(), _values.end()); };
};

} // namespaces
