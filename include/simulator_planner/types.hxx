
#pragma once

#include <memory>

#include <vector>
#include <map>
#include <set>
#include <boost/container/flat_set.hpp>

#include <exception>

/**
 * Custom datatypes and typedefs.
 */
namespace fs0 {
  
  class AtomRange;
	
	template <typename T>
	using Range = std::pair<T, T>;

	// A unique identifier for any of the domain actions.
	typedef unsigned ActionIdx;

	// A sequence of actions is a plan.
	typedef std::vector<ActionIdx> ActionPlan;
	
	// A unique identifier for an atomic goal
	typedef unsigned AtomicGoal;

	typedef std::set<std::string> strSet;
	typedef std::map<int, std::string> strMap;
	typedef std::vector<std::string> strVec;
	typedef std::set<std::pair<std::string, std::string>> pairStrSet;
	typedef std::map<std::string, strVec> mapStrVec;
	
	//A map of atomic goals with their corresponding vector of atom ranges:
	/*
	 * 1 : { [1,2], [3,4], [5,6] }
	 * 2 : { [1,2], [3,4], [5,6] }
	 * ...
	 */
	typedef std::map<AtomicGoal, std::vector<AtomRange>> Goals; 

	//! An action signature is a list of (positional) parameters with a given type.
	typedef std::vector<std::string> Signature;

	//! The index identifying a state variable.
	typedef unsigned VariableIdx;
	
	//! Custom exceptions
	class UnimplementedFeatureException : public std::runtime_error {
	public:
		UnimplementedFeatureException(const std::string& msg) : std::runtime_error(msg) {}
	};
	
	class InvalidConfiguration : public std::runtime_error {
	public:
		InvalidConfiguration(const std::string& msg) : std::runtime_error(msg) {}
	};

} // namespaces
