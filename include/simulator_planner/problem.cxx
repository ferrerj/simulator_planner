
#include <problem.hxx>
#include <sstream>
//#include <utils/logging.hxx>

namespace fs0 {

std::unique_ptr<Problem> Problem::_instance = nullptr;

Problem::~Problem() {
	for (const auto pointer:_ground) delete pointer;
	for (const auto pointer:_schemata) delete pointer;
}


void Problem::bootstrap() {
	// Safety check
	if (_goal_conditions.empty()) 
		throw std::runtime_error("No goal specification detected. The problem is probably being bootstrapped before having been fully initialized with the per-instance generate() procedure"); 
	
}

std::string Problem::get_action_name(unsigned action) const {
	return _ground[action]->getFullName();
}


std::ostream& Problem::print(std::ostream& os) const { 
	os << "Planning Problem [domain: " << getDomainName() << ", instance: " << getInstanceName() <<  "]" << std::endl;
	
	/*
	os << "Goal Conditions:" << std::endl << "------------------" << std::endl;
	for (const auto formula:getGoalConditions()) os << "\t" << print::formula(*formula) << std::endl;
	os << std::endl;
	
	os << "State Constraints:" << std::endl << "------------------" << std::endl;
	for (const auto formula:getStateConstraints()) os << "\t" << print::formula(*formula) << std::endl;
	os << std::endl;
	
	os << "Ground Actions" << std::endl << "------------------" << std::endl;
	for (const GroundAction::cptr elem:_ground) {
		os << *elem << std::endl;
	}
	os << std::endl;
	*/
	
	return os;
}


} // namespaces
