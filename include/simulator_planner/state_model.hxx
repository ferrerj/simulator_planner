
#pragma once

#include <aptk2/search/interfaces/det_state_model.hxx>
#include <actions/ground_action.hxx>
#include <state.hxx>


namespace fs0 {

class Problem; class Simulator;

class SimulatorStateModel : public aptk::DetStateModel<State, GroundAction> {
public:
	SimulatorStateModel(const Problem& problem, const Simulator& simulator);
	~SimulatorStateModel() {}

	//! Returns initial state of the problem
	State init() const;

	//! Returns true if state is a goal state
	//bool goal(const State& state) const;
	//bool atomic_goal(const State& state, std::vector<AtomRange>& goal_range) const;

	bool goal(const State& state) const;

	//! Returns applicable action set object
	GroundAction::ApplicableSet applicable_actions(const State& state) const;

	//! Returns the state resulting from applying the given action action on the given state
	State next(const State& state, GroundAction::IdType id) const;
	State next(const State& state, const GroundAction::cptr action) const;
	State next(const State& state, const GroundAction& action) const;
	
	const Problem& getProblem() const { return _problem; }
	
	void print(std::ostream &os) const {}

protected:
	// The underlying planning problem.
	const Problem& _problem;
	const Simulator& _simulator;
	
	//! Store a vector with all action IDs for better performance, since all actions will be applicable
	GroundAction::ApplicableSet _applicable;
};

} // namespaces
