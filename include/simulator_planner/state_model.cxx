
#include <state_model.hxx>
#include <problem.hxx>
#include <state.hxx>
#include <algorithm>

#include <simulator/simulator.hxx>

namespace fs0 {

SimulatorStateModel::SimulatorStateModel(const Problem& problem, const Simulator& simulator)
	: _problem(problem), _simulator(simulator), _applicable(problem.getGroundActions().size()) {
	std::iota(_applicable.begin(), _applicable.end(), 0); // i.e. applicable = {0, 1, ..., n-1}
}

State SimulatorStateModel::init() const {
	return _problem.getInitialState(); // This implicitly performs a copy of the state!
}
/*
//Check if an atomic goal has been satisfied
bool SimulatorStateModel::atomic_goal(const State& state, std::vector<AtomRange>& goal_range) const {
	//std::cout << "Size: " << goal_range.size() << std::endl;
	for (AtomRange range:goal_range) {
	    //std::cout << "Check range: " << range.getVariable() << ": " << range.getRange().first << "," << range.getRange().second << std::endl; 
		if (!state.satisfies(range)) return false;
	}
	return true;
}
*/


bool SimulatorStateModel::goal(const State& state) const {
	for (AtomRange range:_problem.getGoalConditions()) {
		if (!state.satisfies(range)) return false;
	}
	return true;
}

/*
//Check if atomic goal has been satisfied. if it is, remove the atomic goal and the actions and...
//the actions? Is it necessary? Or maybe delet the model for which the atomic goal has been satisfied
bool SimulatorStateModel::goal(const State& state) const {
  Goals::iterator agoal;
     for(auto& it: _problem.getGoalConditions()) {
	if(atomic_goal(state, it.second)) {
	  _problem.getGoalConditions().erase(it.first);
	  
	}
	
     }
       
  if(!_problem.getGoalConditions().empty()) return false;
  else return true;
      
}
*/

State SimulatorStateModel::next(const State& state, GroundAction::IdType id) const {
	return next(state, *(_problem.getGroundActions()[id]));
} 

State SimulatorStateModel::next(const State& state, const GroundAction::cptr action) const {
	return next(state, *action);
}

State SimulatorStateModel::next(const State& state, const GroundAction& action) const {
	return _simulator.execute(state, action);
}

GroundAction::ApplicableSet SimulatorStateModel::applicable_actions(const State& state) const {
	return _applicable; // All actions are applicable!
}

} // namespaces

