#include <problem_loader.hxx>
#include <state.hxx>
#include <simulator/gazebo/discrete_simulator.hxx>
#include <simulator/gazebo/continuous_simulator.hxx>
#include <simulator/gazebo/komodo_simulator.hxx>
#include <search/runner.hxx>
#include <search/search.hxx>
#include <utils/config.hxx>
#include <problem.hxx>
#include <utils/utils.hxx>
#include <types.hxx>
#include <atom_range.hxx>
#include <simulator/gazebo/robot.hxx>
#include <simulator/gazebo/plan_performer.hxx>


int main(int argc, char** argv) {
  
 
    clock_t tStart = clock();  

  
 ros::init(argc,argv,"simulator_planning"); 
 
 
 
 std::string home = System::get_home_folder();
 std::string catkin = "/catkin_ws/src/simulator_planner";
 std::string filename = home+catkin+"/data/robot.json";

/*
 
 fs0::Config::init(home+catkin+"/data/config.json");
 const fs0::Config& config = fs0::Config::instance();
 float v_trans = config.getOption<float>("engine.v_trans");
 float v_rot = config.getOption<float>("engine.v_rot");
 const float time = config.getOption<float>("engine.deltaT");
 const int discr = config.getOption<int>("engine.discretization");
 float factor = config.getOption<float>("engine.factor");
 
 std::cout << "Delta Time: " << time << std::endl;


  fs0::ProblemLoader pl;
   std::string str_joints = home +catkin+"/data/problem_joints.json";
  std::vector<std::string> joints = pl.loadUsedJoints(str_joints);
 
  
 fs0::ContinuousGazeboSimulator simulator(v_trans,v_rot,time, discr, config);
 simulator.set_joints(joints);
 simulator.get_models_from_sim();

fs0::strVec grasping;
grasping.push_back("a_robot_big::gripper_big::gr");
grasping.push_back("a_robot_big::gripper_big::gl");
simulator.set_graspingLinks(grasping);
 
 


 auto init = simulator.get_current_state(true, true); 
 
 std::cout << init << std::endl;


 
 std::cout << "Init state size: " << init.size() << std::endl;

  std::string str = home + catkin + "/data/problem";
  
 fs0::Problem::setInstance(pl.loadProblem(str, std::move(init)));

 const fs0::Problem& problem = fs0::Problem::getInstance();
 
 std::cout << "GOAL: " << std::endl;
for(unsigned i = 0; i < problem.getGoalConditions().size(); ++i)
  std::cout << problem.getGoalConditions().at(i) << std::endl;

	
  fs0::engines::SearchUtils::report_stats(problem);
	
  fs0::SimulatorStateModel model(problem, simulator);
  fs0::engines::SearchUtils::instantiate_search_engine_and_run(model, config, 1000, home+catkin+"/data");
  */
 
 
  std::string states = home + catkin + "/data/states_plan.out";
  std::string actions = home + catkin + "/data/plan.out";
  
  fs0::PlanPerformer p(actions);
  p.read_action();
  p.perform_plans();
  //p.read_states();
  //p.set_robot_state();
    
  
    printf("Total Time: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

   
 return 0;

 
}




